import { CookieService } from './../services/cookie.service';
import { FavouriteRecipeService } from './../services/favourite-recipe.service';
import { ActivatedRoute } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Recipe } from '../models/Recipe';
import { RecipeService } from '../services/recipe.service';

@Component({
  selector: 'app-show-recipe',
  templateUrl: './show-recipe.component.html',
  styleUrls: ['./show-recipe.component.css']
})
export class ShowRecipeComponent implements OnInit {

  @Input() isLoggedIn = false;
  
  private _recipeSubscription: Subscription;
  public get recipeSubscription(): Subscription {
    return this._recipeSubscription;
  }
  public set recipeSubscription(value: Subscription) {
    this._recipeSubscription = value;
  }
  public recipe: Recipe;

  constructor(
    private recipeService: RecipeService,
    private activatedRoute: ActivatedRoute,
    private cookieService: CookieService,
    private favouriteRecipeService: FavouriteRecipeService
    ) {
  }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(paramMap => {
      let url = paramMap.get('url');
      this.recipe=this.recipeService.getRecipe(url);
      this.recipeSubscription=this.recipeService.recipeChanged.subscribe(recipe => {
        this.recipe=recipe;
        this.setFavouriteButton(this.recipe);
      })
    });

    this.isLoggedIn = this.cookieService.getLoggedIn();
    this.cookieService.loginChanged.subscribe(state => this.isLoggedIn = state);
  }

  addToFavouritesClick(recipe): void {
    var currentButtonAttribute = document.getElementById("star").getAttribute("class");
    if(currentButtonAttribute == "fa fa-star-o fa-3x") {
      document.getElementById("star").setAttribute("class", "fa fa-star fa-3x");
      this.favouriteRecipeService.addRecipeToFavourites(recipe.id);
    }
    else {
      document.getElementById("star").setAttribute("class", "fa fa-star-o fa-3x");
      this.favouriteRecipeService.removeRecipeFromFavourites(recipe.id);
    }
  }

  private setFavouriteButton(recipe: Recipe) {
    if (recipe.isFavourite) {
      document.getElementById("star").setAttribute("class", "fa fa-star fa-3x");
    }
    else {
      document.getElementById("star").setAttribute("class", "fa fa-star-o fa-3x");
    }
  }

  onPersonamountChange() {
    var persAmountInput = document.getElementById("inputPersonAmount") as HTMLInputElement;
    var persAmount = persAmountInput.value;
    this.changeIngredientsAmount(persAmount);
  }

  private changeIngredientsAmount(persAmount: any) {
    document.getElementById("ingredientAmount");
  }
}
