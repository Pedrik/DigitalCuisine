import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-show-steps',
  templateUrl: './show-steps.component.html',
  styleUrls: ['./show-steps.component.css']
})
export class ShowStepsComponent implements OnInit {

  constructor() { }

  @Input() instruction: any;

  ngOnInit(): void {
  }

}
