import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-show-ingredients',
  templateUrl: './show-ingredients.component.html',
  styleUrls: ['./show-ingredients.component.css']
})
export class ShowIngredientsComponent implements OnInit {

  constructor() { }

  @Input() ingredient: any;

  ngOnInit(): void {
  }

}
