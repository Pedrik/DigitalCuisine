import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-show-comments',
  templateUrl: './show-comments.component.html',
  styleUrls: ['./show-comments.component.css']
})
export class ShowCommentsComponent implements OnInit {

  @Input() rating: any;

  constructor() { }

  ngOnInit(): void {
    for (let index = 0; index < this.rating.rating; index++) {
      var para = document.createElement("li");
      para.setAttribute("class","fa fa-star g-pos-rel g-top-1 g-mr-3")
      document.getElementById("ratingRating").appendChild(para);
    }
    for (let index = 0; index < 5 - this.rating.rating; index++) {
      var para = document.createElement("li");
      para.setAttribute("class","fa fa-star-o g-pos-rel g-top-1 g-mr-3")
      document.getElementById("ratingRating").appendChild(para);
    }
  }

}
