import { Recipe } from 'src/app/models/Recipe';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-favourite-recipe',
  templateUrl: './favourite-recipe.component.html',
  styleUrls: ['./favourite-recipe.component.css']
})
export class FavouriteRecipeComponent implements OnInit {

  constructor() { }

  @Input() recipe: any;

  ngOnInit(): void {
  }

}
