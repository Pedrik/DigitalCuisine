import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Recipe } from '../models/Recipe';
import { FavouriteRecipeService } from '../services/favourite-recipe.service';

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.css']
})
export class FavouritesComponent implements OnInit {

  public recipeSubscription: Subscription;
  public recipes: Recipe[];

  private categories: string[] = [""];

  constructor(private favouriteRecipeService: FavouriteRecipeService) {
    this.recipes=this.favouriteRecipeService.getRecipes();
    this.recipeSubscription=this.favouriteRecipeService.recipesChanged.subscribe(recipes => {
      this.recipes=recipes;
    })
  }

  ngOnInit(): void {
  }

  onCheckboxChage(event, value) {
    if (event.path[0].checked) {
      if (this.categories[0] != "")
        this.categories.push(value);
      else
        this.categories[0] = value;
    } 
    if (!event.path[0].checked) {
      let index = this.categories.indexOf(value);
      if (index > -1) {
        this.categories.splice(index, 1);
      }
    }
    if (this.categories.length < 1) {
      this.recipes=this.favouriteRecipeService.getRecipes();
    } else {
      this.favouriteRecipeService.categoriesSearch(this.categories);
    }
  }
}