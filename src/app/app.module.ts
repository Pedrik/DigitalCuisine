import { FiltersPipe } from './pipelines/filters.pipe';
import { AuthService } from './services/auth.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { routing } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { FavouritesComponent } from './favourites/favourites.component';
import { ProfileComponent } from './profile/profile.component';
import { ShowRecipeComponent } from './show-recipe/show-recipe.component';
import { RecipeListComponent } from './recipe-list/recipe-list.component';
import { RecipeListItemComponent } from './recipe-list/recipe-list-item/recipe-list-item.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { ShowIngredientsComponent } from './show-recipe/show-ingredients/show-ingredients.component';
import { ShowStepsComponent } from './show-recipe/show-steps/show-steps.component';
import { ManagementComponent } from './admin/management/management.component';
import { HomeRecipeComponent } from './home/home-recipe/home-recipe.component';
import { FavouriteRecipeComponent } from './favourites/favourite-recipe/favourite-recipe.component';
import { ManagementRecipesComponent } from './admin/management/management-recipes/management-recipes.component';
import { ManagementUsersComponent } from './admin/management/management-users/management-users.component';
import { AddRecipeComponent } from './add-recipe/add-recipe.component';
import { AddUserComponent } from './add-user/add-user.component';
import { ShowCommentsComponent } from './show-recipe/show-comments/show-comments.component';
import { ProfileRecipeComponent } from './profile/profile-recipe/profile-recipe.component';
import { MultiSelectModule } from '@syncfusion/ej2-angular-dropdowns';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModifyRecipeComponent } from './modify-recipe/modify-recipe.component';
import { ModifyUserComponent } from './modify-user/modify-user.component';
import { FridgeComponent } from './fridge/fridge.component';
import { FridgeRecipesComponent } from './fridge/fridge-recipes/fridge-recipes.component';
import { FridgeIngredientsComponent } from './fridge/fridge-ingredients/fridge-ingredients.component';
import { FridgeCategoriesComponent } from './fridge/fridge-categories/fridge-categories.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    FavouritesComponent,
    ProfileComponent,
    ShowRecipeComponent,
    RecipeListComponent,
    RecipeListItemComponent,
    LoginComponent,
    ShowIngredientsComponent,
    ShowStepsComponent,
    ManagementComponent,
    HomeRecipeComponent,
    FavouriteRecipeComponent,
    ManagementRecipesComponent,
    ManagementUsersComponent,
    AddRecipeComponent,
    AddUserComponent, 
    ShowCommentsComponent,
    FiltersPipe,
    ProfileRecipeComponent,
    ModifyRecipeComponent,
    ModifyUserComponent,
    FridgeComponent,
    FridgeRecipesComponent,
    FridgeIngredientsComponent,
    FridgeCategoriesComponent
  ],
  imports: [
    BrowserModule,
    routing,
    HttpClientModule,
    MultiSelectModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function tokenGetter() {
  return localStorage.getItem('token');
}