import { FridgeComponent } from './fridge/fridge.component';
import { ModifyUserComponent } from './modify-user/modify-user.component';
import { ModifyRecipeComponent } from './modify-recipe/modify-recipe.component';
import { AddUserComponent } from './add-user/add-user.component';
import { AddRecipeComponent } from './add-recipe/add-recipe.component';
import { ProfileComponent } from './profile/profile.component';
import { FavouritesComponent } from './favourites/favourites.component';
import { ShowRecipeComponent } from './show-recipe/show-recipe.component';
import { RecipeListComponent } from './recipe-list/recipe-list.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ManagementComponent } from './admin/management/management.component';

import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'recipes-list', component: RecipeListComponent },
  { path: 'show-recipe/:url', component: ShowRecipeComponent },
  { path: 'favourites', component: FavouritesComponent },
  { path: 'user/:username', component: ProfileComponent },
  { path: 'login', component: LoginComponent },
  { path: 'admin', component: ManagementComponent },
  { path: 'add-recipe', component: AddRecipeComponent },
  { path: 'modify-recipe/:url', component: ModifyRecipeComponent },
  { path: 'add-user', component: AddUserComponent },
  { path: 'modify-user/:username', component: ModifyUserComponent },
  { path: 'fridge', component: FridgeComponent }
];

export const routing = RouterModule.forRoot(routes);
