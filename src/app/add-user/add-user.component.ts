import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from './../services/user.service';
import { Credentials } from './../models/auth.models';
import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { checkPassword, validateForm } from '../services/auth.service';
import { Subscription } from 'rxjs';
import { User } from '../models/User';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  @Input() profilePicture = "";
  @Input() username = "";
  @Input() email = "";
  @Input() first_name = "";
  @Input() last_name = "";
  @Input() password = "";
  @Input() passwordVerify = "";
  isAdmin: boolean = false;
  @Input() passwordCriteria = "";

  public userSubscription: Subscription;
  public user: Credentials;

  constructor(
    private sanitizer: DomSanitizer,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    
  }

  image: string | SafeUrl =
    "http://ssl.gstatic.com/accounts/ui/avatar_2x.png";

  updateImage(ev, imageInput: any) {
    this.image = imageInput;
    this.image = this.sanitizer.bypassSecurityTrustUrl(
      window.URL.createObjectURL(ev.target.files[0])
    );
  }

  addUserClick() {
    var u = new User();
    u.picture = this.profilePicture;
    u.username = this.username;
    u.first_name = this.first_name;
    u.last_name = this.last_name;
    u.email = this.email;
    
      u.password = this.password;
      u.passwordVerify = this.passwordVerify;
    
    u.description = "";
    u.admin_privilleges = this.isAdmin;

    if (validateForm(0)) {
      this.userService.addUser(u);
      if (setTimeout(this.userService.getSuccessAdd, 3000)) {
        this.clearData();
      }
    }
    else {
      this.passwordCriteria = checkPassword(this.password);
    }
  }

  private clearData() {
    this.profilePicture = "";
    this.username = "";
    this.email = "";
    this.first_name = "";
    this.last_name = "";
    this.password = "";
    this.passwordVerify = "";
    this.isAdmin = false;
    this.passwordCriteria = "";
  }

  onIsAdminChage(e) {
    let isChecked = e.target.checked;
    if (isChecked) {
      this.isAdmin = true;
    } else {
      this.isAdmin = false;
    }
  }
}
