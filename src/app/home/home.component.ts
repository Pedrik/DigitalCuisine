import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Recipe } from '../models/Recipe';
import { HomeService } from '../services/home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public recipeSubscription: Subscription;
  public recipes: Recipe[];

  constructor(private homeService: HomeService) {
    this.recipes=this.homeService.getRecipes();
    this.recipeSubscription=this.homeService.recipesChanged.subscribe(recipes => {
      this.recipes=recipes;
    })
  }

  ngOnInit(): void {
  }

}
