import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-recipe',
  templateUrl: './home-recipe.component.html',
  styleUrls: ['./home-recipe.component.css']
})
export class HomeRecipeComponent implements OnInit {

  constructor() { }

  @Input() recipe: any;

  ngOnInit(): void {
  }

}
