import { CookieService } from './../services/cookie.service';
import { AuthService, checkPassword, validateForm } from './../services/auth.service';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Credentials } from '../models/auth.models';
import { compileDeclarePipeFromMetadata } from '@angular/compiler';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  public loginSubscription: Subscription;
  invalidLogin: boolean;

  public registerSubscription: Subscription;
  invalidRegister: boolean;

  @Input() username = "";
  @Input() email = "";
  @Input() firstname = "";
  @Input() lastname = "";
  @Input() password = "";
  @Input() verifyPassword = "";

  @Input() passwordCriteria = "";

  constructor(
    private router: Router,
    private authService: AuthService,
    private cookieService: CookieService
  ) { }

  ngOnInit(): void {
    this.clearLogin();
  }

  loginClick() {
    var c = new Credentials();
    c.password = this.password;
    c.username = this.username;
    this.signIn(c);
  }

  private signIn(credentials) {
    if(validateForm(0)){
      this.authService.doLogin(credentials);
      this.loginSubscription = this.authService.jwtTokenChanged.subscribe(result => {
        if (result) {
          this.cookieService.setToken(result);
          this.setIsAdmin();
          this.router.navigate(['/']);
        }
        else
          this.invalidLogin = true;
      });
    }
    else {
      this.passwordCriteria = checkPassword(this.password);
    }
  }

  registerClick() {
    var c = new Credentials();
    c.username = this.username;
    c.password = this.password;
    c.passwordVerify = this.verifyPassword;
    c.email = this.email;
    c.first_name = this.firstname;
    c.last_name = this.lastname;
    this.register(c);
  }

  private register(credentials: Credentials) {
    if(validateForm(1)){
      this.authService.doRegister(credentials);
      this.registerSubscription = this.authService.credChanged.subscribe(result => {
        if (result) {
          let element: HTMLElement = document.getElementById('login-tab') as HTMLElement;
          this.clearLogin();
          element.click();
        }
        else
          this.invalidRegister = true;
      });
    }
    else {
      this.passwordCriteria = checkPassword(this.password);
    }
  }

  private setIsAdmin() {
    this.authService.getIsAdmin();
  }

  private clearLogin() {
    this.username = "";
    this.email = "";
    this.firstname = "";
    this.lastname = "";
    this.password = "";
    this.verifyPassword = "";
    this.passwordCriteria = "";
  }
}