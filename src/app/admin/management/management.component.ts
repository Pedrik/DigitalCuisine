import { UserService } from './../../services/user.service';
import { RecipeService } from './../../services/recipe.service';
import { Credentials } from './../../models/auth.models';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Recipe } from 'src/app/models/Recipe';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.css']
})
export class ManagementComponent implements OnInit {

  public recipeSubscription: Subscription;
  public recipes: Recipe[];

  public userSubscription: Subscription;
  public users: Credentials[];

  constructor(private recipeService: RecipeService, private userService: UserService) {
    this.recipes=this.recipeService.getRecipes();
    this.recipeSubscription=this.recipeService.recipesChanged.subscribe(recipes => {
      this.recipes=recipes;
    })

    this.users=this.userService.getUsers();
    this.userSubscription=this.userService.usersChanged.subscribe(users => {
      this.users=users;
    })
  }

  ngOnInit(): void {
  }

}
