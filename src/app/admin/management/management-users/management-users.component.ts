import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-management-users',
  templateUrl: './management-users.component.html',
  styleUrls: ['./management-users.component.css']
})
export class ManagementUsersComponent implements OnInit {

  constructor(
    private userService: UserService,
    private router: Router
    ) { }

  @Input() user: any;

  ngOnInit(): void {
  }

  deleteUserClick() {
    this.userService.deleteUser(this.user.id);
    //location.reload();
  }
}
