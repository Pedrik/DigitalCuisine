import { Component, Input, OnInit } from '@angular/core';
import { RecipeService } from 'src/app/services/recipe.service';

@Component({
  selector: 'app-management-recipes',
  templateUrl: './management-recipes.component.html',
  styleUrls: ['./management-recipes.component.css']
})
export class ManagementRecipesComponent implements OnInit {

  constructor(
    private recipeService: RecipeService
    ) { }

  @Input() recipe: any;

  ngOnInit(): void {
  }

  deleteRecipeClick() {
    this.recipeService.deleteRecipe(this.recipe.id);
    location.reload();
  }
}
