import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementRecipesComponent } from './management-recipes.component';

describe('ManagementRecipesComponent', () => {
  let component: ManagementRecipesComponent;
  let fixture: ComponentFixture<ManagementRecipesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementRecipesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementRecipesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
