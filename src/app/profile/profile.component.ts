import { User } from './../models/User';
import { AuthService, checkPassword, handleErrorMessage, validateForm } from './../services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { Credentials } from './../models/auth.models';
import { Subscription } from 'rxjs';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public userSubscription: Subscription;
  public user: User;

  @Input() profilePicture = "";
  @Input() username = "";
  @Input() email = "";
  @Input() first_name = "";
  @Input() last_name = "";
  @Input() description = "";

  @Input() newPassword = "";
  @Input() newPasswordVerify = "";
  
  @Input() passwordCriteria = "";

  constructor(
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(paramMap => {
      let username = paramMap.get('username');
      
      this.user = this.userService.getUser(username);
      this.userSubscription=this.userService.userChanged.subscribe(user => {
        this.user=user;
      })
    });
  }

  addRecipeClick() {
    this.router.navigate(['/add-recipe']);
  }

  changeDataClick() {
    this.username = this.user.username;
    this.first_name = this.user.first_name;
    this.last_name = this.user.last_name;
    this.email = this.user.email;
    this.description = this.user.description;
  }

  saveChangesClick() {
    var u: User = new User;
    u.id = this.user.id;
    u.username = this.username;
    u.email = this.email;
    u.first_name = this.first_name;
    u.last_name = this.last_name;
    u.picture = this.user.picture;
    u.description = this.description;
    u.password = this.newPassword;
    u.passwordVerify = this.newPasswordVerify;
    u.admin_privilleges = this.user.admin_privilleges;

    if (validateForm(1)) {
      if (validateForm(0)) {
        this.userService.modifyUser(u);
      }
    }
    else {
      this.passwordCriteria = checkPassword(this.newPassword);
    }
  }
}
