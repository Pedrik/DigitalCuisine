import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { RecipeService } from 'src/app/services/recipe.service';

@Component({
  selector: 'app-profile-recipe',
  templateUrl: './profile-recipe.component.html',
  styleUrls: ['./profile-recipe.component.css']
})
export class ProfileRecipeComponent implements OnInit {

  public recipeSubscription: Subscription;

  constructor(
    private recipeService: RecipeService
    ) { }

  @Input() recipe: any;

  ngOnInit(): void {
  }

  deleteRecipeClick() {
    this.recipeService.deleteRecipe(this.recipe.id);
    this.recipe = this.recipeService.getRecipe(this.recipe.url);
      this.recipeSubscription=this.recipeService.recipesChanged.subscribe(recipe => {
        this.recipe=recipe;
      })
  }
}
