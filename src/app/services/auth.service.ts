import { CookieService, getCookie } from './cookie.service';
import { Credentials } from './../models/auth.models';
import { Subject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginResponse } from '../models/javawebtoken';

declare let alertify: any;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private jwtToken: string;
  public jwtTokenChanged: Subject<string> = new Subject<string>();

  public isAdminChanged: Subject<boolean> = new Subject<boolean>();

  private cred: Credentials = new Credentials();
  public credChanged: Subject<Credentials> = new Subject<Credentials>();

  constructor(
    private http: HttpClient,
    private cookieService: CookieService
    ) { }

  private fetchJavaWebToken(credentials: Credentials): void {
    /*let param: HttpParams = new HttpParams()
      .set("user", credentials.username)
      .set("password", credentials.password);
    var body = JSON.stringify({ "user": credentials.username, "password": credentials.password });*/
    this.http.post<any>(buildUrl("/login"), { "user": credentials.username, "password": credentials.password }).subscribe(
        jwt => {
          this.setJavaWebToken(jwt.token)
        },
        error => {
          handleErrorMessage(error);
        }
      );
  }

  public setJavaWebToken(jwtToken: string): void {
    this.jwtToken = jwtToken;
    this.jwtTokenChanged.next(this.jwtToken);
  }

  public doLogin(credentials: Credentials): string {
    this.fetchJavaWebToken(credentials);
    return this.jwtToken;
  }

  /*logout() {
    this.http.get(buildUrlWithAuth("/logout"), { headers: getHeader() }).subscribe(
      success => {
        if (success == 404) handleSuccessMessage("Successfully loged out!");
      },
      error => {
        handleErrorMessage(error);
      }
    );
  }*/

  public doRegister(credentials: Credentials) {
    this.fetchRegister(credentials);
  }

  private fetchRegister(credentials): void {
    this.http.post<any>(buildUrl("/register"), credentials).subscribe(
      data => {
        this.setRegister(data);
      },
      error => {
        handleErrorMessage(error.error.error);
      }
    );
  }

  public setRegister(cred: Credentials): void {
    this.cred = cred;
    this.credChanged.next(this.cred);
  }

  public getIsAdmin() {
    this.fetchIsAdmin();
  }

  public fetchIsAdmin() {
    this.http.get<any>(buildUrlWithAuth("/me"), { headers: getHeader() }).subscribe(
      data => {
        if (data.username && data.isAdmin) {
          if (data.isAdmin == 1){
            this.cookieService.setAdmin(true);
          } else {
            this.cookieService.setAdmin(false);
          }
        }
        this.cookieService.setUsername(data.username);
      },
      error => {
        handleErrorMessage(error.error.error);
      }
    );
  }
}



export function validateForm(whichOne: number) {
  var form = document.getElementsByClassName('needs-validation')[whichOne] as HTMLFormElement;
  if (form.checkValidity() === false) {
    event.preventDefault();
    event.stopPropagation();
  }
  else {
    return true;
  }
  form.classList.add('was-validated');
}


export function checkPassword(password: string): string {
  var pw = password;
  var returnString = "";

  //check upperCase
  var upperCaseLetters = /[A-Z]/g;
  if (!pw.match(upperCaseLetters)) {
    returnString += "a uppercase letter! ";
  }

  //check lowercase
  var lowerCaseLetters = /[a-z]/g;
  if (!pw.match(lowerCaseLetters)) {
    returnString += "a lowercase letter! ";
  }

  //check number
  var numbers = /[0-9]/g;
  if (!pw.match(numbers)) {
    returnString += "a number! ";
  }

  //check length
  if (pw.length <= 8) {
    returnString += "min length of 8! ";
  }
  return returnString;
}




export function buildUrlWithAuth(path: string): string {
  console.log('sending auth-request');
  return buildUrl(path);
}

export function buildUrl(path: string): string {
  return `https://digitalcuisine.hopto.org:8080` + path;
}

export function handleErrorMessage(error: string): void {
  alertify.error("An error has occured! - " + error).delay(5);
}

export function handleSuccessMessage(success: string): void {
  alertify.success(success).delay(5);
}

export function getHeader(): HttpHeaders {
  var headers_object = new HttpHeaders({
    'Authorization': getCookie("token")
  });
  return headers_object;
}