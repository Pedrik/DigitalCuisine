import { stringify } from '@angular/compiler/src/util';
import { Recipe } from './../models/Recipe';
import { Injectable } from '@angular/core';
import { buildUrl, buildUrlWithAuth, getHeader, handleErrorMessage, handleSuccessMessage } from './auth.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Subject } from 'rxjs';
import { param } from 'jquery';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';


@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  private recipes: Recipe[] = [];
  public recipesChanged: Subject<Recipe[]> = new Subject<Recipe[]>();

  private recipe: Recipe = new Recipe();
  public recipeChanged: Subject<Recipe> = new Subject<Recipe>();

  constructor(private http: HttpClient) { }

  //alle Recipes laden
  private fetchRecipes(): void {
    this.http.get<Recipe[]>(buildUrl("/recipe-list")).subscribe(
      recipes => {
        this.addRecipesUrl(recipes);
        this.setRecipes(recipes);
      },
      error => {
        handleErrorMessage(error.error.error);
      }
    );
  }

  private setRecipes(recipes: Recipe[]): void {
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice());
  }

  public getRecipes(): Recipe[] {
    this.fetchRecipes();
    return this.recipes.slice();
  }
  
  private addRecipesUrl(recipes: Recipe[]) {
    recipes.forEach(recipe => recipe.picture = buildUrl("") + recipe.picture);
  }

  //einzelnes Recipe aufrufen
  private fetchRecipe(url: string): void {
    this.http.get<Recipe>(buildUrl("/recipe/") + url, { headers: getHeader() } ).subscribe(
      recipe => {
        this.addRecipeUrl(recipe);
        this.setRecipe(recipe);
      },
      error => {
        handleErrorMessage(error.error.error);
      }
    );
  }

  public setRecipe(recipe: Recipe): void {
    this.recipe = recipe;
    this.recipeChanged.next(this.recipe);
  }

  public getRecipe(url: string): Recipe {
    this.fetchRecipe(url);
    return this.recipe;
  }

  public addRecipeUrl(recipe: Recipe) {
    recipe.picture = "https://digitalcuisine.hopto.org:8080" + recipe.picture;
  }

  //einzelnes Recipe hinzufügen
  public addRecipe(recipe: Recipe, formData: FormData): void {
    this.http.post<any>(buildUrlWithAuth("/addrecipe"), recipe, { headers: getHeader() }).subscribe(
      response => {
        this.setSuccessAdd(true);
        this.addPictureRecipe(formData, response.recipe_id);
        handleSuccessMessage("Successfully added Recipe!")
      },
      error => {
        handleErrorMessage(error.error.error);
        this.setSuccessAdd(false);
      }
    );
  }

//einzelnes Recipe hinzufügen
public modifyRecipe(recipe: Recipe, formData: FormData): void {
  this.http.post<any>(buildUrlWithAuth("/modifyrecipe"), recipe, { headers: getHeader() }).subscribe(
    response => {
      this.addPictureRecipe(formData, recipe.id);
      handleSuccessMessage("Successfully modified Recipe!");
    },
    error => {
      handleErrorMessage(error.error.error);
    }
  );
}

  /*private setAddedRecipe(recipe: Recipe): void {
    this.recipes.push(recipe);
    this.recipesChanged.next(this.recipes.slice());
  }*/

  //addPictureToRecipe
  public addPictureRecipe(formData: FormData, recipe_id: number) {
    this.http.post<any>(buildUrlWithAuth("/addrecipepicture") + "?recipe_id=" + recipe_id, formData, { headers: getHeader() }).subscribe(response => {
      if (response.statusCode === 200) {
        console.log(response);
      }
    }, er => {
      handleErrorMessage(er.error.error);
    });
  }

  //Recipe löschen
  public deleteRecipe(recipe_id: number) {
    //let param: HttpParams = new HttpParams();
    //param.set("recipe_id", JSON.stringify(recipe_id));
    this.http.post(buildUrlWithAuth("/removerecipe"), { "recipe_id": recipe_id }, { headers: getHeader() }).subscribe(
      response => {
        this.fetchRecipes();
        handleSuccessMessage("Successfully deleted Recipe!");
      },
      error => {
        handleErrorMessage(error.error.error);
      }
    );
  }

  //Recipes Suchoptionen
  //Categories
  private fetchRecipesCategories(categories: string[]): void {
    //let param: HttpParams = new HttpParams();
    //param.set("categories", JSON.stringify(categories));
    this.http.get<Recipe[]>(buildUrlWithAuth("/searchcategorie?categories="+categories.join(',')), { headers: getHeader() }).subscribe(
      recipes => {
        this.addRecipesUrl(recipes);
        this.setRecipes(recipes);
      },
      error => {
        handleErrorMessage(error.error.error);
      }
    );
  }

  public categoriesSearch(categories: string[]) {
    this.fetchRecipesCategories(categories);
  }

  private fetchRecipesSearch(searchword: string): void {
    //let param: HttpParams = new HttpParams();
    //param.set("searchword", searchword);
    this.http.get<Recipe[]>(buildUrlWithAuth("/search?title="+searchword), { headers: getHeader() }).subscribe(
      recipes => {
        this.addRecipesUrl(recipes);
        this.setRecipes(recipes);
      },
      error => {
        handleErrorMessage(error.error.error);
      }
    );
  }

  public search(searchtext: string) {
    this.fetchRecipesSearch(searchtext);
  }

  //Successfull add?
  successAdd = false;
  public setSuccessAdd(sucAdd: boolean) {
    this.successAdd = sucAdd;
  }

  public getSuccessAdd() {
    return this.successAdd;
  }
}