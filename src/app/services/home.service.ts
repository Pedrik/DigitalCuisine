import { Recipe } from './../models/Recipe';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';
import { buildUrl, getHeader, handleErrorMessage } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  private recipes: Recipe[] = [];
  public recipesChanged: Subject<Recipe[]> = new Subject<Recipe[]>();

  private recipe: Recipe = new Recipe();
  public recipeChanged: Subject<Recipe> = new Subject<Recipe>();

  constructor(private http: HttpClient) { }

  private fetchRecipes(): void {
    this.http.get<Recipe[]>(buildUrl("/home"), {headers: getHeader()}).subscribe(
      recipes => {
        this.addRecipesUrl(recipes);
        this.setRecipes(recipes);
      },
      error => {
        handleErrorMessage(error.error.error);
      }
    );
  }

  public setRecipes(recipes: Recipe[]): void {
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice(0, 5));
  }

  public getRecipes(): Recipe[] {
    this.fetchRecipes();
    return this.recipes.slice();
  }

  private addRecipesUrl(recipes: Recipe[]) {
    recipes.forEach(recipe => recipe.picture = buildUrl("") + recipe.picture);
  }
}