import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private token = "";
  public tokenChanged = new Subject<string>();

  private username = "";
  public usernameChanged = new Subject<string>();

  private isLoggedIn = false;
  public loginChanged = new Subject<boolean>();
  
  private isAdmin = false;
  public adminChanged = new Subject<boolean>();

  constructor() {
    let token = localStorage.getItem("token");
    if (token !== null) {
      this.isLoggedIn = token.length > 0;
      this.token = token;
    }
    
    let username = localStorage.getItem("username");
    if (username !== null)
      this.username = username;
    
    let admin = localStorage.getItem("isAdmin");
    if (admin !== null)
      this.isAdmin = admin == "true";
  }

  setToken(token: string) {
    this.token = token;
    localStorage.setItem("token", token);
    this.isLoggedIn = token.length > 0;
    this.loginChanged.next(this.isLoggedIn);
    this.tokenChanged.next(this.token);
  }

  getToken(): string {
    return this.token;
  }

  getLoggedIn(): boolean {
    return this.isLoggedIn;
  }

  setAdmin(state: boolean) {
    this.isAdmin = state;
    localStorage.setItem("isAdmin", this.isAdmin.toString());
    this.adminChanged.next(this.isAdmin);
  }

  getAdmin(): boolean {
    return this.isAdmin;
  }

  setUsername(username: string) {
    this.username = username;
    localStorage.setItem("username", username);
    this.usernameChanged.next(this.username);
  }

  getUsername(): string {
    return this.username;
  }

  clearSession(): void {
    this.token = "";
    localStorage.removeItem("token");
    this.tokenChanged.next(this.token);

    this.isAdmin = false;
    localStorage.removeItem("isAdmin");
    this.adminChanged.next(this.isAdmin);

    this.username = "";
    localStorage.removeItem("username");
    this.usernameChanged.next(this.username);

    this.isLoggedIn = false;
    this.loginChanged.next(this.isLoggedIn);
  }
}
