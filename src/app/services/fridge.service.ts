import { RecipeService } from './recipe.service';
import { HttpClient } from '@angular/common/http';
import { Ingredient, Recipe } from './../models/Recipe';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { buildUrl, getHeader, handleErrorMessage } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class FridgeService {

  private recipes: Recipe[] = [];
  public recipesChanged: Subject<Recipe[]> = new Subject<Recipe[]>();

  private ingredients: string[] = [];
  public ingredientsChanged: Subject<string[]> = new Subject<string[]>();

  private categories: string[] = [];
  public categoriesChanged: Subject<string[]> = new Subject<string[]>();

  constructor(
    private http: HttpClient
    ) { }

  //alle Recipes laden
  private fetchRecipes(): void {
    this.http.get<Recipe[]>(buildUrl("/fridge")).subscribe(
      recipes => {
        this.addRecipesUrl(recipes);
        this.setRecipes(recipes);
      },
      error => {
        handleErrorMessage(error.error.error);
      }
    );
  }

  private setRecipes(recipes: Recipe[]): void {
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice());
  }

  public getRecipes(): Recipe[] {
    this.fetchRecipes();
    return this.recipes.slice();
  }

  getRecipesIngredients(ing: string[]): Recipe[] {
    this.http.get<Recipe[]>(buildUrl("/fridge?ingredients="+ing.join(',')), { headers: getHeader() }).subscribe(
      recipes => {
        this.addRecipesUrl(recipes);
        this.setRecipes(recipes);
      },
      error => {
        handleErrorMessage(error.error.error);
      }
    );
    return this.recipes;
  }

  getRecipesCategories(cat: string[]): Recipe[] {
    this.http.get<Recipe[]>(buildUrl("/fridge?categories="+cat.join(',')+"&ingredients=kindergarten"), { headers: getHeader() }).subscribe(
      recipes => {
        this.addRecipesUrl(recipes);
        this.setRecipes(recipes);
      },
      error => {
        handleErrorMessage(error.error.error);
      }
    );
    return this.recipes;
  }

  private addRecipesUrl(recipes: Recipe[]) {
    recipes.forEach(recipe => recipe.picture = buildUrl("") + recipe.picture);
  }

  //alle Ingredients laden
  private fetchIngredients(): void {
    this.http.get<string[]>(buildUrl("/ingredients")).subscribe(
      ingredients => {
        this.setIngredients(ingredients);
      },
      error => {
        handleErrorMessage(error.error.error);
      }
    );
  }

  private setIngredients(ingredients: string[]): void {
    this.ingredients = ingredients;
    this.ingredientsChanged.next(this.ingredients.slice());
  }

  public getIngredients(): string[] {
    this.fetchIngredients();
    return this.ingredients.slice();
  }

  public ingredientsData: string[] = [
    'Karotte',
    'Schwein',
    'Kinder',
    'Tobias'
  ];

  //alle Categories laden
  private fetchCategories(): void {
    var categories = this.categoriesData;
    this.setCategories(categories);
  }

  private setCategories(categories: string[]): void {
    this.categories = categories;
    this.categoriesChanged.next(this.categories.slice());
  }

  public getCategories(): string[] {
    this.fetchCategories();
    return this.categories.slice();
  }

  public categoriesData: string[] = [
    'breakfast',
    'lunch',
    'dinner',
   'casserole',
    'roast',
    'bread',
    'dip',
    'dressing',
    'ice_cream',
    'snack',
    'fish',
    'vegetable',
    'drink',
    'grills',
    'low_calorie',
    'sweets',
    'cake',
    'jam',
    'noodle',
    'pancake',
    'pizza',
    'cooky',
    'salad',
    'sauce',
    'soup',
    'wok'
  ];
}
