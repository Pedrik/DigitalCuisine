import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Recipe } from '../models/Recipe';
import { buildUrl, buildUrlWithAuth, getHeader, handleErrorMessage, handleSuccessMessage } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class FavouriteRecipeService {

  private recipes: Recipe[] = [];
  public recipesChanged: Subject<Recipe[]> = new Subject<Recipe[]>();

  private recipe: Recipe = new Recipe();
  public recipeChanged: Subject<Recipe> = new Subject<Recipe>();

  constructor(private http: HttpClient) { }

  //alle Recipes laden
  private fetchRecipes(): void {
    this.http.get<Recipe[]>(buildUrlWithAuth("/favourites"), { headers: getHeader() }).subscribe(
      recipes => {
        this.addRecipesUrl(recipes);
        this.setRecipes(recipes);
      },
      error => {
        handleErrorMessage(error.error.error);
      }
    );
  }

  private setRecipes(recipes: Recipe[]): void {
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice());
  }

  public getRecipes(): Recipe[] {
    this.fetchRecipes();
    return this.recipes.slice();
  }

  //nur Recipes zu bestimmten Categorien
  private fetchRecipesCategories(categories: string[]): void {
    //var param = new HttpParams();
    //param = param.append('categories', categories.join(','));
    this.http.get<Recipe[]>(buildUrlWithAuth("/favouritessearchcategorie?categories="+categories.join(',')), { headers: getHeader() }).subscribe(
      recipes => {
        this.addRecipesUrl(recipes);
        this.setRecipes(recipes);
      },
      error => {
        handleErrorMessage(error.error.error);
      }
    );
  }

  public categoriesSearch(categories: string[]): void {
    this.fetchRecipesCategories(categories);
  }

  private addRecipesUrl(recipes: Recipe[]) {
    recipes.forEach(recipe => recipe.picture = buildUrl("") + recipe.picture);
  }

  //Recipe zu favourites hinzufügen
  public addRecipeToFavourites(id: number): void {
    //let param: HttpParams = new HttpParams();
    //param.set("recipe_id", JSON.stringify(recipe_id));
    this.http.post<Recipe>(buildUrlWithAuth("/addfavourite"), { "recipe_id": id }, { headers: getHeader() }).subscribe(
      success => {
        handleSuccessMessage("Added Recipe to Favourite!");
      },
      error => {
        handleErrorMessage(error.error.error);
      }
    );
  }

  //Recipe von favourites entfernen
  public removeRecipeFromFavourites(id: number): void {
    //let param: HttpParams = new HttpParams();
    //param.set("recipe_id", JSON.stringify(recipe_id));
    this.http.post(buildUrlWithAuth("/removefavourite"), { "recipe_id": 0 }, { headers: getHeader() }).subscribe(
      success => {
        handleSuccessMessage("Removed Recipe from Favourite!");
        this.recipesChanged.next();
      },
      error => {
        handleErrorMessage(error.error.error);
      }
    );
  }
}
