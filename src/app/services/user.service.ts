import { RecipeService } from 'src/app/services/recipe.service';
import { User } from './../models/User';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Credentials } from '../models/auth.models';
import { buildUrl, buildUrlWithAuth, getHeader, handleErrorMessage, handleSuccessMessage } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private users: User[] = [];
  public usersChanged: Subject<User[]> = new Subject<User[]>();

  private user: User = new User();
  public userChanged: Subject<User> = new Subject<User>();

  constructor(
    private http: HttpClient,
    private router: Router,
    private recipeService: RecipeService
    ) { }

  //alle Users laden
  private fetchUsers(): void {
    var header: HttpHeaders = getHeader();
    this.http.get<User[]>(buildUrlWithAuth("/users"), { headers: header }).subscribe(
      users => {
        this.setUsers(users);
      },
      error => {
        handleErrorMessage(error.error.error);
      }
    );
  }

  private setUsers(users: User[]): void {
    this.users = users;
    this.usersChanged.next(this.users.slice());
  }

  public getUsers(): User[] {
    this.fetchUsers();
    return this.users.slice();
  }

  //einzelnen User aufrufen
  private fetchUser(username: string): void {
    this.http.get<User>(buildUrlWithAuth("/user/") + username, { headers: getHeader() }).subscribe(
      user => {
        this.setUser(user);
        user.recipes.forEach(recipe => {
        this.recipeService.addRecipeUrl(recipe);
        });
      },
      error => {
        handleErrorMessage(error.error.error);
      }
    );
  }

  public setUser(user: User): void {
    this.user = user;
    this.userChanged.next(this.user);
  }

  public getUser(username: string): User {
    this.fetchUser(username);
    return this.user;
  }

  //einzelnen User hinzufügen
  public addUser(user: User): void {
    this.http.post<User>(buildUrlWithAuth("/adduser"), user, { headers: getHeader() }).subscribe(
      data => {
        this.setSuccessAdd(true);
        handleSuccessMessage("Successfully added User!");
        this.router.navigate["/admin"];
      },
      error => {
        handleErrorMessage(error.error.error);
        this.setSuccessAdd(false);
      }
    );
  }

  /*private setAddedUser(user: User): void {
    this.users.push(user);
    this.usersChanged.next(this.users);
  }*/

  //User bearbeiten
  public modifyUser(user: User): void {
    this.http.post<User>(buildUrlWithAuth("/modifyuser"), user, { headers: getHeader() }).subscribe(
      data => {
        this.setModifiedUser(data);
        handleSuccessMessage("Successfully modified User!");
        this.router.navigate["/admin"];
      },
      error => {
        handleErrorMessage(error.error.error);
      }
    );
  }

  private setModifiedUser(user: User): void {
    this.users.push(user);
    this.usersChanged.next(this.users);
  }

  //User löschen
  public deleteUser(user_id: number) {
    //let param: HttpParams = new HttpParams();
    //param.set("user_id", JSON.stringify(user_id));
    this.http.post(buildUrlWithAuth("/removeuser"), { "user_id": user_id }, { headers: getHeader() })
      .subscribe(
        response => {
          handleSuccessMessage("Successfully deleted User!");
          this.fetchUsers();
        },
        error => {
          handleErrorMessage(error.error.error);
        }
      );
  }

  //Successfull add?
  successAdd = false;
  public setSuccessAdd(sucAdd: boolean) {
    this.successAdd = sucAdd;
  }

  public getSuccessAdd() {
    return this.successAdd;
  }

  //Successfull mod?
  successMod = false;
  public setSuccessMod(sucMod: boolean) {
    this.successAdd = sucMod;
  }

  public getSuccessMod() {
    return this.successMod;
  }
}
