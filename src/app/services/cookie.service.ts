import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CookieService {

  public tokenChanged = new Subject<string>();

  private username = "";
  public usernameChanged = new Subject<string>();

  private isLoggedIn = false;
  public loginChanged = new Subject<boolean>();

  private isAdmin = false;
  public adminChanged = new Subject<boolean>();

  constructor() {
    let token = getCookie("token");
    if (token !== null) {
      this.isLoggedIn = token.length > 0;
    }
    
    let username = getCookie("username");
    if (username !== null)
      this.username = username;
    
    let admin = getCookie("isAdmin");
    if (admin !== null)
      this.isAdmin = admin == "true";
  }

  setToken(token: string) {
    this.setCookie("token", token, 7);
    this.isLoggedIn = token.length > 0;
    this.loginChanged.next(this.isLoggedIn);
    this.tokenChanged.next(this.getToken());
  }

  setAdmin(state: boolean) {
    this.isAdmin = state;
    this.setCookie("isAdmin", state, 7);
    this.adminChanged.next(this.isAdmin);
  }

  setUsername(username: string) {
    this.username = username;
    this.setCookie("username", username, 7);
    this.usernameChanged.next(this.username);
  }

  getToken() {
    return getCookie("token");
  }

  getAdmin() {
    return this.isAdmin;
  }

  getUsername() {
    return this.username;
  }

  getLoggedIn(): boolean {
    return this.isLoggedIn;
  }

  setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  deleteAllCookies(): void {
    document.cookie.split(";").forEach(
      function (c) {
        document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/");
      });
      this.tokenChanged.next(this.getToken());
      this.isAdmin = false;
      this.adminChanged.next(this.getAdmin());
      this.username = "";
      this.usernameChanged.next(this.getUsername());
      this.isLoggedIn = false;
      this.loginChanged.next(this.getLoggedIn());
  }
}

export function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}