import { FridgeComponent } from './../fridge.component';
import { Component, Input, OnInit } from '@angular/core';
import { columnSelectionComplete } from '@syncfusion/ej2-angular-grids';

@Component({
  selector: 'app-fridge-ingredients',
  templateUrl: './fridge-ingredients.component.html',
  styleUrls: ['./fridge-ingredients.component.css']
})
export class FridgeIngredientsComponent implements OnInit {

  @Input() ingredient: any;

  constructor( private fridgeComponent: FridgeComponent) { }

  ngOnInit(): void {
  }

  onIngredientChange(ev) {
    this.fridgeComponent.onIngChange(ev);
  }
}
