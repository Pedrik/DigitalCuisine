import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FridgeIngredientsComponent } from './fridge-ingredients.component';

describe('FridgeIngredientsComponent', () => {
  let component: FridgeIngredientsComponent;
  let fixture: ComponentFixture<FridgeIngredientsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FridgeIngredientsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FridgeIngredientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
