import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-fridge-recipes',
  templateUrl: './fridge-recipes.component.html',
  styleUrls: ['./fridge-recipes.component.css']
})
export class FridgeRecipesComponent implements OnInit {

  @Input() recipe: any;

  constructor() { }

  ngOnInit(): void {
  }

}
