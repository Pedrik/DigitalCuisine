import { RecipeService } from 'src/app/services/recipe.service';
import { FridgeService } from './../services/fridge.service';
import { Ingredient } from './../models/Recipe';
import { Component, OnInit } from '@angular/core';
import { Recipe } from '../models/Recipe';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-fridge',
  templateUrl: './fridge.component.html',
  styleUrls: ['./fridge.component.css']
})
export class FridgeComponent implements OnInit {

  public recipes: Recipe[];
  public recipeSubscription: Subscription;
  public ingredients: string[];
  public ingredientsSubscription: Subscription;
  public categories: string[];
  public categorieSubscription: Subscription;

  private ings: string[] = [""];
  private cats: string[] = [""];

  constructor(private fridgeService: FridgeService, private recipeService: RecipeService) { 
    this.recipes=this.recipeService.getRecipes();
    this.recipeSubscription=this.fridgeService.recipesChanged.subscribe(recipes => {
      this.recipes=recipes;
    });

    this.ingredients=this.fridgeService.getIngredients();
    this.ingredientsSubscription=this.fridgeService.ingredientsChanged.subscribe(ingredients => {
      this.ingredients=ingredients;
    });

    this.categories=this.fridgeService.getCategories();
    this.categorieSubscription=this.fridgeService.categoriesChanged.subscribe(categories => {
      this.categories=categories;
    });
  }

  ngOnInit(): void {
  }

  setRecipes() {
    this.recipes = this.fridgeService.getRecipes();
  }

  setRecipesIngredients(ing: string[]) {
    this.recipes = this.fridgeService.getRecipesIngredients(ing);
  }

  setRecipesCategories(cat: string[]) {
    this.recipes = this.fridgeService.getRecipesCategories(cat);
  }

  onIngChange(ev) {
    if (ev.path[0].checked) {
      if (this.ings[0] != ""){
       
        this.ings.push(ev.target.value);
        }
      else{
        this.ings[0] = ev.target.value;
      
      }
    } 
    if (!ev.path[0].checked) {
      let index = this.ings.indexOf(ev.target.value);
      if (index > -1) {
        this.ings.splice(index, 1);
      }
    }
    if (this.ings.length < 1) {
      this.setRecipes();
    } else {
      this.setRecipesIngredients(this.ings);
    }
  }

  onCatChange(ev) {
    if (ev.path[0].checked) {
      if (this.cats[0] != ""){
       
        this.cats.push(ev.target.value);
        }
      else{
        this.cats[0] = ev.target.value;
      
      }
    } 
    if (!ev.path[0].checked) {
      let index = this.cats.indexOf(ev.target.value);
      if (index > -1) {
        this.cats.splice(index, 1);
      }
    }
    if (this.cats.length < 1) {
      this.setRecipes();
    } else {
      this.setRecipesCategories(this.cats);
    }
  }

}
