import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FridgeCategoriesComponent } from './fridge-categories.component';

describe('FridgeCategoriesComponent', () => {
  let component: FridgeCategoriesComponent;
  let fixture: ComponentFixture<FridgeCategoriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FridgeCategoriesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FridgeCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
