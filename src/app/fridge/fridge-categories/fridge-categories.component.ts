import { Component, Input, OnInit } from '@angular/core';
import { FridgeComponent } from '../fridge.component';

@Component({
  selector: 'app-fridge-categories',
  templateUrl: './fridge-categories.component.html',
  styleUrls: ['./fridge-categories.component.css']
})
export class FridgeCategoriesComponent implements OnInit {

  @Input() categorie: any;

  constructor(private fridgeComponent: FridgeComponent) { }

  ngOnInit(): void {
  }

  onCategoriesChange(ev) {
    this.fridgeComponent.onCatChange(ev);
  }
}
