import { Ingredient } from './../models/Recipe';
import { Recipe } from 'src/app/models/Recipe';
import { Component, Input, ViewEncapsulation, OnInit, ViewChild, ElementRef  } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { RecipeService } from '../services/recipe.service';
import { validateForm } from '../services/auth.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-modify-recipe',
  templateUrl: './modify-recipe.component.html',
  styleUrls: ['./modify-recipe.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ModifyRecipeComponent implements OnInit {

  @Input() title = "";
  @Input() author = "";
  @Input() person_amount = 0;
  @Input() from_duration = 0;
  @Input() to_duration = 0;
  @Input() picture = "";
  
  @ViewChild('UploadFileInput', { static: false }) uploadFileInput: ElementRef;
  fileUploadForm: FormGroup;
  fileInputLabel: string;
  image: string | SafeUrl = "";
  public recipeSubscription: Subscription;
  public recipe: Recipe;

  public oldRecipe: Recipe;

  constructor(
    private sanitizer: DomSanitizer,
    private recipeService: RecipeService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute
    ) { }

  ngOnInit(): void {
    this.fileUploadForm = this.formBuilder.group({
      uploadedImage: ['']
    });
    this.activatedRoute.paramMap.subscribe(paramMap => {
      let recipe_url = paramMap.get('url');
      this.recipe = this.recipeService.getRecipe(recipe_url);
      this.recipeSubscription = this.recipeService.recipeChanged.subscribe(recipe => {
        this.setRecipeData(recipe);
      })
    });
  }

  private setRecipeData(recipe: Recipe) {
    this.oldRecipe = recipe;

    this.title = recipe.title;
    this.person_amount = recipe.person_amount;
    this.image = recipe.picture;
    this.to_duration = recipe.to_duration;
    this.from_duration = recipe.from_duration;

    this.selectCategories();
    this.selectTools();
    this.selectDifficulty();
    this.selectIngredients();
    this.selectInstructions();
  }

  modifyRecipeClick() {
    var r = new Recipe();
    r.id = this.oldRecipe.id;
    r.title = this.title;
    r.person_amount = this.person_amount;
    r.from_duration = this.from_duration;
    r.to_duration = this.to_duration;
    r.difficulty = this.getDifficulty();
    r.categories = this.getCategories();
    r.ingredients = this.getIngredients();
    r.instruction = this.getCookingInstructions();
    r.tools = this.getTools();

    console.log(r);

    if(validateForm(0)){
      this.recipeService.modifyRecipe(r, this.getPicture());
      this.clearPicture();
    }
  }

  //Select Recipe Values

  private selectCategories(): void {
    this.oldRecipe.categories.forEach(element => {
      let para = document.createElement("li");
      para.setAttribute("id","categoriesOld");
      para.innerText = element;

      let close = document.createElement('button');
      close.setAttribute("type","button");
      close.setAttribute("class","close");
      close.addEventListener('click', function () {
        var tr = document.getElementById("existingCategories");
        tr.removeChild(para);
      });
      let spanClose = document.createElement('span');
      spanClose.innerHTML="&times;";
      
      close.appendChild(spanClose);
      para.appendChild(close);

      document.getElementById("existingCategories").appendChild(para);
    });
  }

  private selectTools(): void {
    this.oldRecipe.tools.forEach(element => {
      let para = document.createElement("li");
      para.setAttribute("id","toolsOld");
      para.innerText = element;

      let close = document.createElement('button');
      close.setAttribute("type","button");
      close.setAttribute("class","close");
      close.addEventListener('click', function () {
        var tr = document.getElementById("existingTools");
        tr.removeChild(para);
      });
      let spanClose = document.createElement('span');
      spanClose.innerHTML="&times;";
      
      close.appendChild(spanClose);
      para.appendChild(close);

      document.getElementById("existingTools").appendChild(para);
    });
  }

  private selectDifficulty(): void {
    switch(this.oldRecipe.difficulty) {
      case "beginner":
        document.getElementById("btnradio1").setAttribute("checked","checked");
        break;
      case "advanced":
        document.getElementById("btnradio2").setAttribute("checked","checked");
        break;
      case "professional":
        document.getElementById("btnradio3").setAttribute("checked","checked");
        break;
    }
  }

  private selectIngredients(): void {
    this.oldRecipe.ingredients.forEach(element => {
      let para = document.createElement("td");
      para.setAttribute("class", "p-0 pb-1 input-group");
      para.setAttribute("name", "Ingredient");

      let input1 = document.createElement('input');
      input1.setAttribute("type", "text");
      input1.setAttribute("class", "form-control mb-1 w-25");
      input1.setAttribute("name", "name");
      input1.setAttribute("placeholder", "Name");
      input1.setAttribute("required", null);
      input1.value = element.label;

      let input2 = document.createElement('input');
      input2.setAttribute("type", "number");
      input2.setAttribute("class", "form-control mb-1");
      input2.setAttribute("name", "amount");
      input2.setAttribute("placeholder", "Amount");
      input2.setAttribute("required", null);
      input2.value = ""+ element.amount;

      let select = document.createElement('select');
      select.setAttribute("id", "einheit");
      select.setAttribute("class", "custom-select mr-1");
      select.setAttribute("name", "measurement");
      this.addOldMeasurments(select, element.measurement);

      let close = document.createElement('button');
      close.setAttribute("type","button");
      close.setAttribute("class","close");
      close.addEventListener('click', function () {
        var tr = document.getElementById("ingredients");
        tr.removeChild(para);
      });

      let spanClose = document.createElement('span');
      spanClose.innerHTML="&times;";

    close.appendChild(spanClose);

      para.appendChild(input1);
      para.appendChild(input2);
      para.appendChild(select);
      para.append(close);
      document.getElementById("ingredients").appendChild(para);
    })
  }

  private selectInstructions(): void {
    this.oldRecipe.instruction.forEach(element => {
      this.addOldStep(element);
    })
  }

  //Get Recipe Values

  private getCategories(): string[] {
    var cateDoc = document.getElementById("categories-list");
    var categories = cateDoc.innerText.split(', ');

    var oldCat = document.getElementById("existingCategories");
    var oldCatArray = oldCat.innerText.split("\n");
    var test = true;

    oldCatArray.forEach(element => {
      if(test) {
        categories.push(element);
        test=false;
      }
      else {
        test=true;
      }
    });
    
    return categories;
  }

  private getTools(): string[] {  
    var toolDoc = document.getElementById("tools-list");
    var tools = toolDoc.innerText.split(', ');

    var oldTool = document.getElementById("existingTools");
    var oldToolArray = oldTool.innerText.split("\n");
    var test = true;

    oldToolArray.forEach(element => {
      if(test) {
        tools.push(element);
        test=false;
      }
      else {
        test=true;
      }
    });

    return tools;
  }

  private getCookingInstructions(): string[] {
    var cIns: string[] = [];
    document.getElementsByName("cookingInstruction").forEach(
      function (cookingInstruction: HTMLInputElement) {
        cIns.push(cookingInstruction.value);
      }
    );
    return cIns;
  }

  private getIngredients(): Ingredient[] {
    var iIns: Ingredient[] = [];
    document.getElementsByName("Ingredient").forEach(
      function (ingredient: HTMLElement) {
        var i = new Ingredient();
        var array = Array.prototype.slice.call(ingredient.children)
        array.forEach(element => {
          switch (element.name) {
            case "name":
              i.label = element.value;
              break;
            case "amount":
              i.amount = element.value;
              break;
            case "measurement":
              i.measurement = element.value;
              break;
          }
        });
        iIns.push(i);
      }
    );
    return iIns;
  }

  private getDifficulty(): string {
    var diff: string = "";
    if ((document.getElementById('btnradio1') as HTMLInputElement).checked) {
      diff = (document.getElementById('btnradio1') as HTMLInputElement).value;
    } else if ((document.getElementById('btnradio2') as HTMLInputElement).checked) {
      diff = (document.getElementById('btnradio2') as HTMLInputElement).value;
    } else if ((document.getElementById('btnradio3') as HTMLInputElement).checked) {
      diff = (document.getElementById('btnradio3') as HTMLInputElement).value;
    }
    return diff;
  }

  //Picture

  getPicture(): FormData {
    const formData = new FormData();
    formData.append('Image', this.fileUploadForm.get('uploadedImage').value);
    //formData.append('imageName', this.fileUploadForm.get('uploadedImage').value.name);
    formData.append('agentId', '007');
    return formData;
  }

  clearPicture() {
    // Reset the file input
    this.uploadFileInput.nativeElement.value = "";
    this.fileInputLabel = undefined;
  }

  onFileSelect(event) {
    const file = event.target.files[0];
    this.updateImage(file);
    this.fileInputLabel = file.name;
    this.fileUploadForm.get('uploadedImage').setValue(file);
  }

  updateImage(file: any) {
    this.image = this.sanitizer.bypassSecurityTrustUrl(
      window.URL.createObjectURL(file)
    );
  }

  //add HTML-Elemente

  addStep(): void {
    let para = document.createElement("li");
    para.setAttribute("class", "pl-2 pt-2");

    let newP = document.createElement('input');
    newP.setAttribute("name", "cookingInstruction");
    newP.setAttribute("type", "text");
    newP.setAttribute("class", "form-control");
    newP.setAttribute("placeholder", "Step...");
    newP.setAttribute("required", null);

    para.appendChild(newP);
    document.getElementById("instruction").appendChild(para);
  }

  addOldStep(step: string): void {
    let para = document.createElement("li");
    para.setAttribute("class", "pl-2 pt-2");

    let newP = document.createElement('input');
    newP.setAttribute("name", "cookingInstruction");
    newP.setAttribute("type", "text");
    newP.setAttribute("class", "form-control");
    newP.setAttribute("placeholder", "Step...");
    newP.setAttribute("required", null);
    newP.value = step;

    para.appendChild(newP);
    document.getElementById("instruction").appendChild(para);
  }

  addIngredient(iName, iAmount, iMeasurement): void {
    let para = document.createElement("td");
    para.setAttribute("class", "p-0 pb-1 input-group");
    para.setAttribute("name", "Ingredient");

    let input1 = document.createElement('input');
    input1.setAttribute("type", "text");
    input1.setAttribute("class", "form-control mb-1 w-25");
    input1.setAttribute("name", "name");
    input1.setAttribute("placeholder", "Name");
    input1.setAttribute("required", null);
    input1.setAttribute("value", iName.value);

    let input2 = document.createElement('input');
    input2.setAttribute("type", "number");
    input2.setAttribute("class", "form-control mb-1");
    input2.setAttribute("name", "amount");
    input2.setAttribute("placeholder", "Amount");
    input2.setAttribute("required", null);
    input2.setAttribute("value", iAmount.value);

    let select = document.createElement('select');
    select.setAttribute("id", "einheit");
    select.setAttribute("class", "custom-select mr-1");
    select.setAttribute("name", "measurement");
    this.addMeasurments(select, iMeasurement.value);

    let close = document.createElement('button');
    close.setAttribute("type","button");
    close.setAttribute("class","close");
    close.addEventListener('click', function () {
      var tr = document.getElementById("ingredients");
      tr.removeChild(para);
    });

    let spanClose = document.createElement('span');
    spanClose.innerHTML="&times;";

    close.appendChild(spanClose);

    para.appendChild(input1);
    para.appendChild(input2);
    para.appendChild(select);
    para.appendChild(close);
    document.getElementById("ingredients").appendChild(para);
    this.clearIngredientInput();
  }

  clearIngredientInput() {
    var iName = document.getElementById("iName") as HTMLInputElement;
    var iAmount = document.getElementById("iAmount") as HTMLInputElement;
    iName.value = "";
    iAmount.value = "";
  }

  addMeasurments(select: HTMLElement, selected: string) {
    let option1 = document.createElement('option');
    option1.setAttribute("value", "kilo");
    option1.innerHTML = "kilo";

    let option2 = document.createElement('option');
    option2.setAttribute("value", "gram");
    option2.innerHTML = "gram";

    let option3 = document.createElement('option');
    option3.setAttribute("value", "litre");
    option3.innerHTML = "litre";

    let option4 = document.createElement('option');
    option4.setAttribute("value", "millilitre");
    option4.innerHTML = "millilitre";

    let option5 = document.createElement('option');
    option5.setAttribute("value", "dekagram");
    option5.innerHTML = "dekagram";

    let option6 = document.createElement('option');
    option6.setAttribute("value", "pound");
    option6.innerHTML = "pound";

    let option7 = document.createElement('option');
    option7.setAttribute("value", "ounces");
    option7.innerHTML = "ounces";

    let option8 = document.createElement('option');
    option8.setAttribute("value", "piece");
    option8.innerHTML = "piece";

    switch(selected) {
      case "kilo":
        option1.setAttribute("selected", null);
        break;
      case "gram":
        option2.setAttribute("selected", null);
        break;
      case "litre":
        option3.setAttribute("selected", null);
        break;
      case "millilitre":
        option4.setAttribute("selected", null);
        break;
      case "dekagram":
        option5.setAttribute("selected", null);
        break;
      case "pound":
        option6.setAttribute("selected", null);
        break;
      case "ounces":
        option7.setAttribute("selected", null);
        break;
      case "piece":
        option8.setAttribute("selected", null);
        break;
    }

    select.appendChild(option1);
    select.appendChild(option2);
    select.appendChild(option3);
    select.appendChild(option4);
    select.appendChild(option5);
    select.appendChild(option6);
    select.appendChild(option7);
    select.appendChild(option8);
  }

  addOldMeasurments(select: HTMLElement, selected: string) {
    let option1 = document.createElement('option');
    option1.setAttribute("value", "kilo");
    option1.innerHTML = "kilo";
    if(selected == "kilo") {
      option1.setAttribute("selected", null);
    }

    let option2 = document.createElement('option');
    option2.setAttribute("value", "gram");
    option2.innerHTML = "gram";
    if(selected == "gram") {
      option2.setAttribute("selected", null);
    }

    let option3 = document.createElement('option');
    option3.setAttribute("value", "litre");
    option3.innerHTML = "litre";
    if(selected == "litre") {
      option3.setAttribute("selected", null);
    }

    let option4 = document.createElement('option');
    option4.setAttribute("value", "millilitre");
    option4.innerHTML = "millilitre";
    if(selected == "millilitre") {
      option4.setAttribute("selected", null);
    }

    let option5 = document.createElement('option');
    option5.setAttribute("value", "dekagram");
    option5.innerHTML = "dekagram";
    if(selected == "dekagram") {
      option5.setAttribute("selected", null);
    }

    let option6 = document.createElement('option');
    option6.setAttribute("value", "pound");
    option6.innerHTML = "pound";
    if(selected == "pound") {
      option6.setAttribute("selected", null);
    }

    let option7 = document.createElement('option');
    option7.setAttribute("value", "ounces");
    option7.innerHTML = "ounces";
    if(selected == "ounces") {
      option7.setAttribute("selected", null);
    }

    let option8 = document.createElement('option');
    option8.setAttribute("value", "piece");
    option8.innerHTML = "piece";
    if(selected == "piece") {
      option8.setAttribute("selected", null);
    }
    

    select.appendChild(option1);
    select.appendChild(option2);
    select.appendChild(option3);
    select.appendChild(option4);
    select.appendChild(option5);
    select.appendChild(option6);
    select.appendChild(option7);
    select.appendChild(option8);
  }

  //Dropdowns für Recipe

  public toolsData: Object[] = [
    { Id: 'cutting_board', Tool: 'cutting_board' },
    { Id: 'vegetable_peeler', Tool: 'vegetable_peeler' },
    { Id: 'salad_spinner', Tool: 'salad_spinner' },
    { Id: 'potato_masher', Tool: 'potato_masher' },
    { Id: 'can_opener', Tool: 'can_opener' },
    { Id: 'colander', Tool: 'colander' }
  ];
  // maps the appropriate column to fields property
  public fieldsTool: Object = { text: 'Tool', value: 'Id' };
  // set the placeholder to MultiSelect input element
  public placeHolderTools: string = 'Select Tools'; 

  public categoriesData: Object[] = [
    { Id: 'breakfast', Categorie: 'breakfast' },
    { Id: 'lunch', Categorie: 'lunch' },
    { Id: 'dinner', Categorie: 'dinner' },
    { Id: 'casserole', Categorie: 'casserole' },
    { Id: 'roast', Categorie: 'roast' },
    { Id: 'bread', Categorie: 'bread' },
    { Id: 'dip', Categorie: 'dip' },
    { Id: 'dressing', Categorie: 'dressing' },
    { Id: 'ice_cream', Categorie: 'ice_cream' },
    { Id: 'snack', Categorie: 'snack' },
    { Id: 'fish', Categorie: 'fish' },
    { Id: 'vegetable', Categorie: 'vegetable' },
    { Id: 'drink', Categorie: 'drink' },
    { Id: 'grills', Categorie: 'grills' },
    { Id: 'low_calorie', Categorie: 'low_calorie' },
    { Id: 'sweets', Categorie: 'sweets' },
    { Id: 'cake', Categorie: 'cake' },
    { Id: 'jam', Categorie: 'jam' },
    { Id: 'noodle', Categorie: 'noodle' },
    { Id: 'pancake', Categorie: 'pancake' },
    { Id: 'pizza', Categorie: 'pizza' },
    { Id: 'cooky', Categorie: 'cooky' },
    { Id: 'salad', Categorie: 'salad' },
    { Id: 'sauce', Categorie: 'sauce' },
    { Id: 'soup', Categorie: 'soup' },
    { Id: 'wok', Categorie: 'wok' }
  ];
  // maps the appropriate column to fields property
  public fieldsCategorie: Object = { text: 'Categorie', value: 'Id' };
  // set the placeholder to MultiSelect input element
  public placeHolderCategories: string = 'Select Categories';    
  // set the type of mode for how to visualized the selected items in input element.
  public default : string = 'Default';

}
