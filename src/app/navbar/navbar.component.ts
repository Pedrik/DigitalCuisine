import { Router } from '@angular/router';
import { CookieService } from './../services/cookie.service';
import { RecipeService } from './../services/recipe.service';
import { AuthService, handleSuccessMessage } from './../services/auth.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Input() isLoggedIn = false;
  @Input() isAdmin = false;
  @Input() username = "";

  constructor(
    private cookieService: CookieService,
    private authService: AuthService,
    private recipeService: RecipeService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.isLoggedIn = this.cookieService.getLoggedIn();
    this.cookieService.loginChanged.subscribe(state => this.isLoggedIn = state);

    this.isAdmin = this.cookieService.getAdmin();
    this.cookieService.adminChanged.subscribe(state => this.isAdmin = state);
    console.log(this.isAdmin);

    this.username = this.cookieService.getUsername();
    this.cookieService.usernameChanged.subscribe(username => this.username = username);
  }

  logout(): void {
    this.router.navigate(['/']);
    this.cookieService.deleteAllCookies();
    handleSuccessMessage("Successfully loged out!")
  }

  searchRecipes(searchtext: string): void {
    if (location.pathname != "/recipes-list" && location.pathname != "/favourites" && location.pathname != "/admin") {
      this.router.navigate(['/recipes-list']);
    }
    this.recipeService.search(searchtext);
  }
}