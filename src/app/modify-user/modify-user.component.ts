import { routing } from './../app-routing.module';
import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Credentials } from '../models/auth.models';
import { User } from '../models/User';
import { checkPassword, validateForm } from '../services/auth.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-modify-user',
  templateUrl: './modify-user.component.html',
  styleUrls: ['./modify-user.component.css']
})
export class ModifyUserComponent implements OnInit {

  @Input() profilePicture = "";
  @Input() username = "";
  @Input() email = "";
  @Input() first_name = "";
  @Input() last_name = "";
  @Input() password = "";
  @Input() passwordVerify = "";
  isAdmin: boolean = false;
  modifyUser: boolean = false;
  @Input() passwordCriteria = "";
  oldUser: Credentials;

  public userSubscription: Subscription;
  public user: Credentials;

  constructor(private sanitizer: DomSanitizer,
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(paramMap => {
      let username = paramMap.get('username');
        this.user = this.userService.getUser(username);
        this.userSubscription = this.userService.userChanged.subscribe(user => {
          this.setUserData(user);
          this.oldUser = user;
        })
    });
  }

  private setUserData(user: Credentials) {
    console.log(user);
    this.username = user.username;
    this.email = user.email;
    this.first_name = user.first_name;
    this.last_name = user.last_name;
    this.password = user.password;
    this.passwordVerify = user.passwordVerify;
    if (user.admin_privilleges) {
      this.isAdmin = user.admin_privilleges;
      document.getElementById("isAdmin").setAttribute("checked", null);
    }
  }

  image: string | SafeUrl =
    "http://ssl.gstatic.com/accounts/ui/avatar_2x.png";

  updateImage(ev, imageInput: any) {
    this.image = imageInput;
    this.image = this.sanitizer.bypassSecurityTrustUrl(
      window.URL.createObjectURL(ev.target.files[0])
    );
  }

  modifyUserClick() {
    var u = new User();
    u.id = this.oldUser.id;
    u.picture = this.profilePicture;
    u.username = this.username;
    u.first_name = this.first_name;
    u.last_name = this.last_name;
    u.email = this.email;
    u.password = this.password;
    u.passwordVerify = this.passwordVerify;
    u.admin_privilleges = this.isAdmin;

    if (validateForm(0)) {
      this.userService.modifyUser(u);
      if (setTimeout(this.userService.getSuccessMod, 3000)) {
        this.router.navigate(["/admin"]);
      }
    }
    else {
      this.passwordCriteria = checkPassword(this.password);
    }
  }

  onIsAdminChage(e) {
    let isChecked = e.target.checked;
    if (isChecked) {
      this.isAdmin = true;
    } else {
      this.isAdmin = false;
    }
  }
}
