import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Recipe } from '../models/Recipe';
import { RecipeService } from '../services/recipe.service';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  public recipeSubscription: Subscription;
  public recipes: Recipe[];

  private categories: string[] = [""];

  constructor(private recipeService: RecipeService) {
    this.recipes=this.recipeService.getRecipes();
    this.recipeSubscription=this.recipeService.recipesChanged.subscribe(recipes => {
      this.recipes=recipes;
    });
  }

  ngOnInit(): void {
  }

  onCheckboxChage(event, value) {
    if (event.path[0].checked) {
      if (this.categories[0] != "")
        this.categories.push(value);
      else
        this.categories[0] = value;
    } 
    if (!event.path[0].checked) {
      let index = this.categories.indexOf(value);
      if (index > -1) {
        this.categories.splice(index, 1);
      }
    }
    if (this.categories.length < 1) {
      this.recipes=this.recipeService.getRecipes();
    } else {
      this.recipeService.categoriesSearch(this.categories);
    }
  }
}
