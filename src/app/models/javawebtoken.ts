export class LoginResponse {
    public success: number | undefined;
    public token: string | undefined;
    public isAdmin: boolean | undefined;
    public username: string | undefined;
}