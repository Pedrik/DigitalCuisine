export class Recipe {
  public id: number | undefined;
  public url: string | undefined;
  public title: string | undefined;
  public author: Author | undefined;
  public categories: string[] | undefined;
  public picture: string | undefined;
  public ingredients: Ingredient[] | undefined;
  public instruction: string[] | undefined;
  public person_amount: number | undefined;
  public from_duration: number | undefined;
  public to_duration: number | undefined;
  public tools: string[] | undefined;
  public difficulty: string | undefined;
  public ratings: Rating[] | undefined;
  public isFavourite: boolean | undefined;
}

export class Author {
  public id: number | undefined;
  public username: string | undefined;
}

export class Rating {
  public username: string | undefined;
  public rating: number | undefined;
  public comment: string | undefined;
}

export class Ingredient {
  public label: string | undefined;
  public amount: number | undefined;
  public measurement: string | undefined;
}