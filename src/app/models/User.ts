import { Recipe } from 'src/app/models/Recipe';
export class User {
    public id: number;
    public username: string;
    public email: string;
    public password: string;
    public passwordVerify: string;
    public first_name: string;
    public last_name: string;
    public picture: string;
    public description: string;
    public admin_privilleges: boolean;
    public recipes: Recipe[];
}