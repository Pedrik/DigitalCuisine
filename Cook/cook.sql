CREATE TABLE users(
    user_id int(8) not null auto_increment,
    username varchar(16) not null unique,
    email varchar(255) not null unique,
    password varchar(255) not null,
    first_name varchar(255) not null,
    last_name varchar(255) not null,
    picture tinyint(1) not null default 0,
    description varchar(5000),
    admin_privilleges tinyint(1) not null default 0,
    primary key(user_id)
);

INSERT INTO users (username, email, password, first_name, last_name, picture, description, admin_privilleges)
VALUES ('franzi23', 'franziska.bauer@gmail.com', 'passwort', 'Franziska', 'Bauer', 0, 'Willkommen auf meiner Seite!', 0),
('kochmeister', 'felixschneider@aon.at', 'passwort', 'Felix', 'Schneider', 1, "Der Kochmeister zeigt euch wie's geht", 0),
('admin01', 't.steiner@htlkrems.at', 'passwort', 'Tobias', 'Steiner', 0, 'Beep Boop dein Account ist weg', 1);

CREATE TABLE e_categories(
    parent_category_id int default null,
    category_id int auto_increment,
    category_label varchar(32) not null,
    primary key(category_id),
    constraint fk_cat_parent_cat
        foreign key(parent_category_id)
        references e_categories(category_id)
);

INSERT INTO e_categories(category_label)
VALUES ('breakfast'), ('lunch'), ('dinner');

INSERT INTO e_categories(parent_category_id, category_label)
VALUES (1, 'casserole'), (1, 'roast'), (1, 'bread'), (1, 'dip'), (1, 'dressing'), (1, 'ice_cream'), (1, 'snack'), (1, 'fish'), (1, 'vegetable'), (1, 'drink'), (1, 'grills'), 
(1, 'low-calorie'), (1, 'sweets'), (1, 'cake'), (1, 'jam'), (1, 'noodle'), (1, 'pancake'), (1, 'pizza'), (1, 'cooky'), (1, 'salad'), (1, 'sauce'), (1, 'soup'), (1, 'wok');

INSERT INTO e_categories(parent_category_id, category_label)
VALUES (2, 'casserole'), (2, 'roast'), (2, 'bread'), (2, 'dip'), (2, 'dressing'), (2, 'ice_cream'), (2, 'snack'), (2, 'fish'), (2, 'vegetable'), (2, 'drink'), (2, 'grills'), 
(2, 'low-calorie'), (2, 'sweets'), (2, 'cake'), (2, 'jam'), (2, 'noodle'), (2, 'pancake'), (2, 'pizza'), (2, 'cooky'), (2, 'salad'), (2, 'sauce'), (2, 'soup'), (2, 'wok');

INSERT INTO e_categories(parent_category_id, category_label)
VALUES (3, 'casserole'), (3, 'roast'), (3, 'bread'), (3, 'dip'), (3, 'dressing'), (3, 'ice_cream'), (3, 'snack'), (3, 'fish'), (3, 'vegetable'), (3, 'drink'), (3, 'grills'), 
(3, 'low-calorie'), (3, 'sweets'), (3, 'cake'), (3, 'jam'), (3, 'noodle'), (3, 'pancake'), (3, 'pizza'), (3, 'cooky'), (3, 'salad'), (3, 'sauce'), (3, 'soup'), (3, 'wok');

CREATE TABLE e_difficulties(
    difficulty varchar(16) not null,
    primary key(difficulty)
);

INSERT INTO e_difficulties(difficulty)
VALUES ('beginner'), ('advanced'), ('professional');

CREATE TABLE recipes(
    recipe_id int not null auto_increment,
    url_id varchar(64) not null,
    title varchar(255) not null,
    author_id int(8),
    picture tinyint(1) not null default 0,
    instruction text not null,
    person_amount int(2) not null default 1,
    from_duration time not null,
    to_duration time not null,
    difficulty varchar(16) not null,
    primary key(recipe_id),
    constraint fk_recipe_author
        foreign key(author_id)
        references users(user_id),
    constraint fk_recipe_difficulty
        foreign key(difficulty)
        references e_difficulties(difficulty)
);

INSERT INTO recipes(url_id, title, author_id, picture, instruction, person_amount, from_duration, to_duration, difficulty)
VALUES ('23kl4j2d09', 'Fischsuppe', 1, 0, 'Den Fisch bei Raumtemperatur kochen und dannach Suppe hinzufügen', 4, '00:30', '01:00', 'beginner'),
('i34r20fj2c', 'Wildschweinbraten', 2, 1, 'Das Wilschwein über offenes Feuer hängen und verzehren', 8, '02:00', '03:00', 'advanced'),
('dfj2309fx5', 'Ratatouille', 1, 0, 'Man nehme eine Ratte und brate sie auf Holzkohle', 1, '00:15', '00:20', 'professional');

CREATE TABLE recipe_has_categories(
    recipe_id int not null,
    category_id int not null,
    primary key(recipe_id, category_id),
    constraint fk_rhc_recipe
        foreign key(recipe_id)
        references recipes(recipe_id),
    constraint fk_rhc_category
        foreign key(category_id)
        references e_categories(category_id)
);

INSERT INTO recipe_has_categories (recipe_id, category_id)
VALUES (1, 8), (1, 19), (2, 2), (3, 11);

CREATE TABLE e_measurements(
    measurement varchar(12) not null,
    primary key(measurement)
);

INSERT INTO e_measurements(measurement)
VALUES ('kilo'), ('gram'), ('litre'), ('dekagram'), ('pound'), ('ounces'), ('piece');

CREATE TABLE recipe_has_ingredients(
    recipe_id int not null,
    amount int(8) not null,
    measurement varchar(8) not null,
    label varchar(64),
    primary key(recipe_id, label),
    constraint fk_ingredient_measurement
        foreign key(measurement)
        references e_measurements(measurement),
    constraint fk_ingredient_recipe
        foreign key(recipe_id)
        references recipes(recipe_id)
);

INSERT INTO recipe_has_ingredients(recipe_id, amount, measurement, label)
VALUES (1, 1, 'piece', 'Fisch'), (1, 5, 'litre', 'Suppe'), (2, 1, 'piece', 'Wildschwein'), (3, 1, 'piece', 'Ratte');

CREATE TABLE recipe_has_tools(
    recipe_id int not null,
    label varchar(64),
    primary key(recipe_id, label),
    constraint fk_tool_recipe
        foreign key(recipe_id)
        references recipes(recipe_id)
);

INSERT INTO recipe_has_tools(recipe_id, label)
VALUES (1, 'Topf'), (2, 'Feuer'), (3, 'Holzkohle');

CREATE TABLE recipe_has_ratings(
    recipe_id int not null,
    user_id int(8),
    rating tinyint(1) not null,
    comment varchar(1000),
    primary key(recipe_id, user_id),
    constraint fk_rating_recipe
        foreign key(recipe_id)
        references recipes(recipe_id),
    constraint fk_rating_user
        foreign key(user_id)
        references users(user_id)
);

INSERT INTO recipe_has_ratings(recipe_id, user_id, rating, comment)
VALUES (1, 3, 4, 'Bla'), (2, 1, 1, 'Beschreibung zu ungenau'), (3, 2, 10, 'Ratte war ausgezeichnet');

CREATE TABLE user_has_favourites(
    recipe_id int not null,
    user_id int(8) not null,
    primary key(recipe_id, user_id),
    constraint fk_uhf_recipe
        foreign key(recipe_id)
        references recipes(recipe_id),
    constraint fk_uhf_user
        foreign key(user_id)
        references users(user_id)
);

INSERT INTO user_has_favourites(recipe_id, user_id)
VALUES (1, 1), (1,2), (3, 2);