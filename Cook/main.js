const express = require('express');
const mysql = require('mysql-await');
const https = require('https');
const http = require('http');
const { response } = require('express');
const bcrypt = require('bcryptjs');
const saltRounds = 10;
var jwt = require('jsonwebtoken');
var randomToken = require('random-token');
var bodyParser = require('body-parser');
var cors = require('cors');
const multer = require('multer');
var fileExtension = require('file-extension');
const path = require('path');
const glob = require('glob');
var fs = require('fs');

var privateKey  = fs.readFileSync('sslcert/server.key', 'utf8');
var certificate = fs.readFileSync('sslcert/server.crt', 'utf8');
var credentials = {key: privateKey, cert: certificate};

const db = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'cook',
});
  
db.connect();

var app = express();
app.use(cors({origin: 'http://localhost:4200'}));
app.use(bodyParser.json({limit: '50mb'}));
app.use(express.static('pictures'));
app.use('/pictures', express.static('pictures'));

var httpServer = http.createServer(app).listen(8080);
var httpsServer = https.createServer(credentials, app).listen(8443);

app.get('/', get_home);
app.get('/home', get_home);
app.get('/recipe-list', get_recipes);
app.get('/recipe/:url', get_recipe);
app.get('/user/:username', get_user);
app.get('/users', get_users);
app.post('/adduser', post_adduser);
app.post('/removeuser', post_removeuser); 
app.post('/removeuserall', post_removeuserall);
app.post('/modifyuser', post_modifyuser);
app.post('/addrecipe', post_addrecipe);
app.post('/removerecipe', post_removerecipe);
app.post('/modifyrecipe', post_modifyrecipe);
app.post('/addcomment', post_addcomment);
app.get('/comment', get_comment);
app.get('/favourites', get_favourites);
app.post('/addfavourite', post_addfavourites);
app.post('/removefavourite', post_removefavourites);
app.get('/me', get_me);
app.post('/login', post_login);
app.post('/register', post_register);
app.get('/search', get_search);
app.get('/searchcategorie', get_search_categorie);
app.get('/favouritessearchcategorie', get_favourites_search_categorie);
app.get('/ingredients', get_ingredients);
app.get('/fridge', get_fridge);
app.post('/adduserpicture', multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, 'pictures/user/')
        },
        filename: async function (req, file, cb) {
            var obj = req.query;
            var user = await getMyUser(req);
            var uUser = await getUser(obj.user_id);

            if(uUser == null) cb(new Error(`User does not exist`));
            else if(user == null) cb(new Error(`You need to be logged in.`));
            else if(!(user.isAdmin == 1 || user.id == uUser.id)) cb(new Error(`No permission!`));
            else if(!file) cb(new Error('Please upload a file'));
            else if(!file.originalname.match(/.(jpg|jpeg|png)$/)) cb(new Error('Please upload JPG and PNG images only!'));
            else {
                let result = await sql(`UPDATE users SET picture = 1 WHERE user_id = ?`, [`${obj.id}`]);
                cb(null, obj.id + '.' + fileExtension(file.originalname))
            }
        }
    }),
    limits: {
        fileSize: 10000000 //10Mb
    }
}).single('Image'), (req, res) => res.send({}));
app.post('/removeuserpicture', async function(req, res){
    var obj = req.body;
    var user = await getMyUser(req);

    if(user == null) return error(res, `You need to be logged in.`);
    else if(!(user.isAdmin == 1 || user.id == obj.id)) return error(res, `No permission!`);
    else {
        var uUser = await getUser(obj.id);
        if(uUser == null) return error(res, `User does not exist`);
        let result = await sql(`UPDATE users SET picture = 0 WHERE user_id = ?`, [`${obj.id}`]);
    }
});
app.post('/addrecipepicture', multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, 'pictures/recipe/')
        },
        filename: async function (req, file, cb) {
            var obj = req.query;
            var user = await getMyUser(req);
            var recipe = await getRecipe(obj.recipe_id);

            if(recipe == null) cb(new Error(`Recipe does not exist.`));
            else if(user == null) cb(new Error(`You need to be logged in.`));
            else if(!(user.isAdmin == 1 || user.id == recipe.author.id)) cb(new Error(`No permission!`));
            else if(!file) cb(new Error('Please upload a file'));
            else if(!file.originalname.match(/.(jpg|jpeg|png)$/)) cb(new Error('Please upload JPG and PNG images only!'));
            else {
                let result = await sql(`UPDATE recipes SET picture = 1 WHERE recipe_id = ?`, [`${recipe.id}`]);
                cb(null, recipe.id + '.' + fileExtension(file.originalname))
            }
        }
    }),
    limits: {
        fileSize: 10000000 //10Mb
    }
}).single('Image'), (req, res) => res.send({}));
app.post('/removerecipepicture', async function(req, res){
    var obj = req.body;
    var user = await getMyUser(req);
    var recipe = await getRecipe(obj.recipe_id);

    if(user == null) return error(res, `You need to be logged in.`);
    else if(!(user.isAdmin == 1 || user.id == obj.author.id)) return error(res, `No permission!`);
    else {
        var uRecipe = await getRecipe(obj.id);
        if(uRecipe == null) return error(res, `Recipe does not exist`);
        let result = await sql(`UPDATE recipes SET picture = 0 WHERE recipe_id = ?`, [`${obj.id}`]);
    }
});
app.get('*', function(req, res) {return error(res, "Page doesn't exist.", 404)});

async function get_home(req, res){
    let myUser = await getMyUser(req);
    let recipes = [];
    for(var element of await sql(`SELECT recipe_id FROM recipes ORDER BY recipe_id DESC LIMIT 5;`)) {
        recipes.push(await getRecipe(element.recipe_id, myUser));
    };
    res.json(recipes);
}

async function get_recipes(req, res){
    var myUser = await getMyUser(req);

    let recipes = [];
    for(var element of await sql(`SELECT recipe_id FROM recipes ORDER BY RAND();`)) {
        recipes.push(await getRecipe(element.recipe_id, myUser));
    }; 
    res.json(recipes);
}

async function get_recipe(req, res){
    var user = await getMyUser(req);
    let result = await sql(`SELECT recipe_id FROM recipes WHERE url_id = ?;`, [`${req.params.url}`]);
    if(result.length == 0) return error(res, "Recipe doesn't exits.", 404);
    else res.json(await getRecipe(result[0].recipe_id, user));
}

async function getRecipe(recipe_id, user = null){
    let sql_values = [`${recipe_id}`];
    let result;
    let recipe = {}
    try{
        result = await sql(`SELECT * FROM recipes r LEFT JOIN users u ON r.author_id = u.user_id WHERE recipe_id = ?;`, sql_values);
        recipe.id = result[0].recipe_id;
        recipe.title = result[0].title;
        recipe.author = {
            id: result[0].user_id,
            username: result[0].username
        }
        recipe.url = result[0].url_id;
        recipe.instruction = result[0].instruction.split(';');
        recipe.person_amount = result[0].person_amount;
        recipe.from_duration = result[0].from_duration;
        recipe.to_duration = result[0].to_duration;
        recipe.difficulty = result[0].difficulty;

        result = await sql(`SELECT picture FROM recipes WHERE recipe_id = ?;`, sql_values);
        recipe.picture = '/pictures/recipe/default.png';
        if(result[0].picture > 0){
            glob('./pictures/recipe/' +  recipe.id + '.*', {}, (err, files) => {
                if(files.length > 0)
                    recipe.picture = files[0].substring(1, files[0].length);
            });
        }

        recipe.tools = [];
        for(var element of await sql(`SELECT label FROM recipes r LEFT JOIN recipe_has_tools t ON t.recipe_id = r.recipe_id WHERE r.recipe_id = ?`, sql_values)) {
            recipe.tools.push(element.label);
        }

        recipe.categories = [];
        for(var element of await sql(`SELECT c.category_label as label FROM recipes r LEFT JOIN recipe_has_categories rc ON rc.recipe_id = r.recipe_id LEFT JOIN e_categories c ON c.category_id = rc.category_id WHERE r.recipe_id = ?`, sql_values)) {
            recipe.categories.push(element.label);
        }

        recipe.ratings = [];
        for(var element of await sql(`SELECT u.username, r.rating, r.comment FROM recipe_has_ratings r LEFT JOIN users u ON r.user_id = u.user_id WHERE r.recipe_id = ?`, sql_values)) {
            recipe.ratings.push({
                username: element.username,
                rating: element.rating,
                comment: element.comment
            });
        }

        recipe.ingredients = [];
        for(var element of await sql(`SELECT * FROM recipe_has_ingredients WHERE recipe_id = ?`, sql_values)) {
            recipe.ingredients.push({
                label: element.label,
                amount: element.amount,
                measurement: element.measurement
            });
        }

        if(user != null)
            recipe.isFavourite = await sql(`SELECT EXISTS(SELECT 1 FROM user_has_favourites WHERE user_id = ? AND recipe_id = ?) as value;`, [`${user.id}`,`${recipe.id}`])
            .then(result => result[0].value == 1 ? true : false);
    } catch (error) {
        console.log(error)
    } finally{
        return recipe;
    }
}

async function get_user(req, res){
    var myUser = await getMyUser(req);
    let result = await sql(`SELECT user_id FROM users WHERE username = ?;`, [`${req.params.username}`]);
    if(result.length == 0) return error(res, "User doesn't exist.", 404);
    else res.json(await getUser(result[0].user_id, myUser));
}

async function get_users(req, res){
    var myUser = await getMyUser(req);
    let result = await sql(`SELECT user_id FROM users;`);
    if(result.length == 0) return error(res, "No users found.", 404);
    let output = []
    for(var u of result) output.push(await getUser(u.user_id, myUser));
    return res.json(output);
}

async function getUser(user_id, myUser = null){
    let user = {}
    let result = await sql(`SELECT * FROM users WHERE user_id = ?;`, [`${user_id}`]);
    if (result.length == 0) return null;
    user.id = result[0].user_id;
    user.username = result[0].username;
    user.email = result[0].email;
    user.first_name = result[0].first_name;
    user.last_name = result[0].last_name;
    user.description = result[0].description;
    user.admin = result[0].admin_privilleges;
    user.recipes = [];
    result = await sql(`SELECT picture FROM users WHERE user_id = ?;`, [`${user.id}`]);
    user.picture = '/pictures/user/default.png';
    if(result[0].picture > 0){
        glob('./pictures/user/' +  user.id + '.*', {}, (err, files) => {
            if(files.length > 0)
                recipe.picture = files[0].substring(1, files[0].length);
        });
    }
    for (var r of await sql(`SELECT recipe_id FROM recipes WHERE author_id = ?;`, [`${user.id}`])) {
        user.recipes.push(await getRecipe(r.recipe_id, myUser));
    }
    return user;
}

async function post_adduser(req, res){
    var obj = req.body;
    var myUser = await getMyUser(req);

    if(myUser == null) return error(res, `You need to be logged in.`, 403);
    else if(!(myUser.isAdmin == 1)) return error(res, `No permission!`, 403);
    else if((obj.username && obj.email && obj.password && obj.passwordVerify && obj.first_name && obj.last_name ) == null) return error(res, `Uncomplete object.`);
    else if((obj.username && obj.email && obj.password && obj.passwordVerify && obj.first_name && obj.last_name) == "") return error(res, `Fill in all blanks.`);
    else if(await isUsernameTaken(obj.username)) return error(res, 'Username is already taken.');
    else if(!(await isUsernameValid(obj.username))) return error(res, 'Username is invalid');
    else if(await isEmailTaken(obj.email)) return error(res, 'Email is already in use.');
    else if(!(await isEmailValid(obj.email))) return error(res, 'Email is invalid.');
    else if(obj.password != obj.passwordVerify) return error(res, `Passwords don't match.`);
    else {
        obj.username = (obj.username).toLowerCase();
        obj.email = (obj.email).toLowerCase();
        let passwordHashed = bcrypt.hashSync(obj.password, saltRounds);
        let result = await sql(`INSERT INTO users (username, email, password, first_name, last_name) VALUES (?,?,?,?,?);`, [`${obj.username}`, `${obj.email}`, `${passwordHashed}`, `${obj.first_name}`, `${obj.last_name}`]);
        if(result.affectedRows == 0) return error(res, "Something went wrong!");
    }
    return res.json({});
}

async function post_removeuser(req, res){
    var obj = req.body;
    var myUser = await getMyUser(req);

    if(await sql(`SELECT EXISTS(SELECT 1 FROM users WHERE user_id = ?) as value;`, [`${obj.user_id}`]).then(results => results[0].value == 0)) return error(res, `User doesn't exists!`, 404);
    else if(myUser == null) return error(res, `You need to be logged in.`, 403);
    else if(!(myUser.isAdmin == 1 || myUser.id == obj.user_id)) return error(res, `No permission!`, 403);
    else {
        let result = await sql(`DELETE FROM users WHERE user_id = ?;`, [`${obj.user_id}`]);
        if(result.affectedRows == 0) return error(res, `Something went wrong pls try again later.`);
    }
    return res.json({});
}

async function post_removeuserall(req, res){
    var obj = req.body;
    var myUser = await getMyUser(req);

    if(await sql(`SELECT EXISTS(SELECT 1 FROM users WHERE user_id = ?) as value;`, [`${obj.user_id}`]).then(results => results[0].value == 0)) return error(res, `User doesn't exists!`, 404);
    else if(myUser == null) return error(res, `You need to be logged in.`, 403);
    else if(!(myUser.isAdmin == 1 || myUser.id == user_id)) return error(res, `No permission!`, 403);
    else {
        let result = await sql(`DELETE FROM user_accs WHERE user_id = ?;`, [`${obj.recipe_id}`]);
        if(result.affectedRows == 0) return error(res, `Something went wrong pls try again later.`);
        post_removeuser(req, res);
    }
    return res.json({});
}

async function post_modifyuser(req, res){
    var obj = req.body;
    var myUser = await getMyUser(req);
    let user = await sql(`SELECT * FROM users WHERE user_id = ?;`, [`${obj.id}`]).then(results => results[0]);

    if(await sql(`SELECT EXISTS(SELECT 1 FROM users WHERE user_id = ?) as value;`, [`${obj.id}`]).then(result => result[0].value == 0)) return error(res, `User doesn't exists!`, 404);
    else if(myUser == null) return error(res, `You need to be logged in.`, 403);
    else if(!(myUser.isAdmin == 1 || myUser.id == obj.id)) return error(res, `No permission!`, 403);
    else if((obj.username && obj.email && obj.first_name && obj.last_name && obj.picture && obj.password && obj.passwordVerify) == null) return error(res, `Uncomplete object.`);
    else if((obj.username && obj.email && obj.first_name && obj.last_name && obj.password && obj.passwordVerify) == "") return error(res, `Fill in all blanks.`);
    else if(await isUsernameTaken(obj.username) && user.username != obj.username) return error(res, 'Username is already taken.');
    else if(!(await isUsernameValid(obj.username))) return error(res, 'Username is invalid');
    else if(await isEmailTaken(obj.email) && user.email != obj.email) return error(res, 'Email is already in use.');
    else if(!(await isEmailValid(obj.email))) return error(res, 'Email is invalid.');
    else if(obj.password != obj.passwordVerify) return error(res, 'Passwords do not match!');
    else {
        if(!(obj.isAdmin && myUser.isAdmin))
            obj.isAdmin = 0;
        var passwordHasehd = bcrypt.hashSync(obj.password, saltRounds);
        let result = await sql(`UPDATE users SET username = ?, email = ?, first_name = ?, last_name = ?, description = ?, admin_privilleges = ?, password = ? WHERE user_id = ?`, [`${obj.username}`, `${obj.email}`, `${obj.first_name}`, `${obj.last_name}`, `${obj.description}`, `${obj.isAdmin}`, `${passwordHasehd}`, `${obj.id}`]);
        if(result.affectedRows == 0) return error(res, `Something went wrong pls try again later.`);
    }

    return res.json({});
}

async function get_favourites(req, res){
    var myUser = await getMyUser(req);
    var favourites = [];
    for(var element of await sql(`SELECT recipe_id FROM user_has_favourites WHERE user_id = ?;`, [`${myUser.id}`])) {
        favourites.push(await getRecipe(element.recipe_id, myUser));
    };
    res.json(favourites);
}

async function post_addfavourites(req, res){
    var obj = req.body;
    var myUser = await getMyUser(req);

    let sql_values = [`${obj.recipe_id}`,`${myUser.id}`];
    let result_favourite = await sql(`SELECT recipe_id FROM user_has_favourites WHERE recipe_id = ? AND user_id = ?;`, sql_values);
    let result_recipe = await sql(`SELECT EXISTS(SELECT 1 FROM recipes WHERE recipe_id = ?) as val;`, [ `${obj.recipe_id}`]);
    
    if(result_favourite.length > 0) return error(res, "Recipe is already a favourite!");
    if(result_recipe[0].val == 0) return error(res, "Recipe doesn't exist!");
    else {
        let result = await sql(`INSERT INTO user_has_favourites(recipe_id, user_id) VALUES (?,?);`, sql_values);
        if(result.affectedRows == 0) return error(res, "Something went wrong! Try again later!");
    }    
    return res.json({});
}

async function post_removefavourites(req, res){
    var obj = req.body;
    var myUser = await getMyUser(req);

    let sql_values = [`${obj.recipe_id}`,`${myUser.id}`];
    
    if(await sql(`SELECT EXISTS(SELECT 1 FROM user_has_favourites WHERE recipe_id = ? AND user_id = ?) as value;`, sql_values).then(result => result[0].value == 0)) return error(res, "Recipe is not a favourite!");
    let result = await sql(`DELETE FROM user_has_favourites WHERE recipe_id = ? AND user_id = ?;`, sql_values);
    if(result.affectedRows == 0) error(res, "Something went wrong! Try again later!");   
    return res.json({});
}

async function get_me(req, res){
    var user = await getMyUser(req);
    let userinfo = await sql('SELECT * FROM users WHERE user_id = ?;', [`${user.id}`]);
    user.username = userinfo[0].username;
    res.send(user);
}

async function post_addrecipe(req, res){
    let result = ``;
    var obj = req.body;
    var myUser = await getMyUser(req);
    let response = { };

    while(obj.url_id == null){
        let url_id = randomToken(10);
        result = await sql(`SELECT EXISTS(SELECT 1 FROM recipes WHERE url_id = ?) as val;`, [`${url_id}`]);
        if(result[0].val == 0)
            obj.url_id = url_id;
    }

    obj.instruction = obj.instruction.join(';');
    result = await sql(`INSERT INTO recipes (url_id, title, author_id, instruction, person_amount, from_duration, to_duration, difficulty) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`, [`${obj.url_id}`, `${obj.title}`, `${myUser.id}`, `${obj.instruction}`, `${obj.person_amount}`, `${obj.from_duration}`, `${obj.to_duration}`, `${obj.difficulty}`]);
    obj.id = result.insertId;

    if(obj.id > 0){
        if(obj.tools.length > 0){
            for(var element of obj.tools) {
                result = await sql(`INSERT INTO recipe_has_tools (recipe_id, label) VALUES (?, ?)`, [`${obj.id}`, `${element}`]);
            }
        }
        
        if(obj.categories.length > 0){
            for(var element of obj.categories) {
                result = await sql(`SELECT category_id FROM e_categories WHERE category_label = ?`, [`${element}`]);
                result = await sql(`INSERT INTO recipe_has_categories (recipe_id, category_id) VALUES (?, ?)`, [`${obj.id}`, `${result[0].category_id}`]);
            }
        }
                
        if(obj.ingredients.length > 0){
            for(var element of obj.ingredients) {
                result = await sql(`INSERT INTO recipe_has_ingredients (recipe_id, label, amount, measurement) VALUES (?, ?, ?, ?)`, [`${obj.id}`, `${element.label}`, `${element.amount}`, `${element.measurement}`]);
            }
        }
        
    }

    result = await sql(`SELECT EXISTS(SELECT 1 FROM recipes WHERE recipe_id = ?) as value;`, [`${obj.id}`]);
    if(result[0].value == 1) return res.json({recipe_id = obj.id});
    else return error(res, "Something went wrong! Please try again later.");
}

async function post_removerecipe(req, res){
    var obj = req.body;
    var user = await getMyUser(req);

    let result_recipe = await sql(`SELECT EXISTS(SELECT 1 FROM recipes WHERE recipe_id = ?) as value;`, [`${obj.recipe_id}`]);
    let result_author = await sql(`SELECT author_id FROM recipes WHERE recipe_id = ?;`, [`${obj.recipe_id}`]);

    if(result_recipe[0].value == 0) return error(res, `Recipe doesn't exists!`, 404);
    else if(user == null) return error(res, `You need to be logged in.`, 403);
    else if(!(user.isAdmin == 1 || user.id == result_author[0].author_id)) return error(res, `No permission!`, 403);
    else {
        let result = await sql(`DELETE FROM recipes WHERE recipe_id = ?`, [`${obj.recipe_id}`]);
        if(result.affectedRows == 0) return error(res, `Something went wrong please try again later.`);
    }

    return res.json({});
}

async function post_modifyrecipe(req, res){
    let result = ``;
    var obj = req.body;
    var user = await getMyUser(req);

    let result_recipe = await sql(`SELECT EXISTS(SELECT 1 FROM recipes WHERE recipe_id = ?) as value;`, [`${obj.id}`]);
    let result_author = await sql(`SELECT author_id FROM recipes WHERE recipe_id = ?;`, [`${obj.id}`]);

    if(result_recipe[0].value == 0) return error(res, `Recipe doesn't exists!`, 404);
    else if(user == null) return error(res, `You need to be logged in.`, 403);
    else if(!(user.isAdmin == 1 || user.id == result_author[0].author_id)) return error(res, `No permission!`, 403);
    else {
        obj.instruction = obj.instruction.join(';');
        result = await sql(`UPDATE recipes SET title = ?, instruction = ?, person_amount = ?, from_duration = ?, to_duration = ?, difficulty = ? WHERE recipe_id = ?`, [`${obj.title}`, `${obj.instruction}`, `${obj.person_amount}`, `${obj.from_duration}`, `${obj.to_duration}`, `${obj.difficulty}`, `${obj.id}`]);
        if(result.affectedRows == 0) return error(res, `Something went wrong please try again later.`);

        result = await sql(`DELETE FROM recipe_has_tools WHERE recipe_id = ?;`, [`${obj.id}`]);
        if(obj.tools.length > 0){
            for(var element of obj.tools) {
                result = await sql(`INSERT INTO recipe_has_tools (recipe_id, label) VALUES (?, ?)`, [`${obj.id}`, `${element}`]);
            }
        }
        
        result = await sql(`DELETE FROM recipe_has_categories WHERE recipe_id = ?;`, [`${obj.id}`]);
        if(obj.categories.length > 0){
            for(var element of obj.categories) {
                result = await sql(`SELECT category_id FROM e_categories WHERE category_label = ?`, [`${element}`]);
                result = await sql(`INSERT INTO recipe_has_categories (recipe_id, category_id) VALUES (?, ?)`, [`${obj.id}`, `${result[0].category_id}`]);
            }
        }
        
        result = await sql(`DELETE FROM recipe_has_ingredients WHERE recipe_id = ?;`, [`${obj.id}`]);
        if(obj.ingredients.length > 0){
            for(var element of obj.ingredients) {
                result = await sql(`INSERT INTO recipe_has_ingredients (recipe_id, label, amount, measurement) VALUES (?, ?, ?, ?)`, [`${obj.id}`, `${element.label}`, `${element.amount}`, `${element.measurement}`]);
            }
        }
    }
    return res.json({});
}

async function post_addcomment(req, res){
    var obj = req.body;
    var myUser = await getMyUser(req);

    if(myUser == null) return error(res, `You need to be logged in.`, 403);
    else if((obj.recipe_id && obj.comment) == null) return error(res, `Uncomplete object.`);
    else if((obj.recipe_id && obj.comment) == "") return error(res, `Fill in all blanks.`);
    else {
        if(await sql(`SELECT EXISTS(SELECT 1 FROM recipe_has_ratings WHERE recipe_id = ? AND user_id = ?) as value;`, [`${obj.recipe_id}`, `${myUser.id}`]).then(results => results[0].value == 0)){
            let result = await sql(`INSERT INTO recipe_has_ratings (user_id, recipe_id, comment) VALUES (?,?,?);`, [`${myUser.id}`, `${obj.recipe_id}`, `${obj.comment}`]);
            if(result.affectedRows == 0) return error(res, "Something went wrong!");
        } else {
            let result = await sql(`UPDATE recipe_has_ratings SET comment = ? WHERE user_id = ? AND recipe_id = ?;`, [`${obj.comment}`, `${myUser.id}`, `${obj.recipe_id}`]);
            if(result.affectedRows == 0) return error(res, "Something went wrong!");
        }
        
    }
    return res.json({});
}

async function get_comment(req, res){
    var obj = req.body;
    var myUser = await getMyUser(req);

    if(myUser == null) return error(res, `You need to be logged in.`, 403);
    else if((obj.recipe_id) == null) return error(res, `Uncomplete object.`);
    else if((obj.recipe_id) == "") return error(res, `Fill in all blanks.`);
    else {
        var result = await sql(`SELECT * FROM recipe_has_ratings WHERE recipe_id = ? AND user_id = ?;`, [`${obj.recipe_id}`, `${myUser.id}`]);
        if(result.length == 0) return error(res, "Something went wrong!");
        return res.json({
            comment: result[0].comment
        });
    }
}

async function post_login(req, res){
    var obj = req.body;
    let info = await getUserInformation(obj.user);
    
    if((obj.user && obj.password) == null) return error(res, `Uncomplete object.`);
    else if((obj.user && obj.password) == "") return error(res, `Fill in all blanks.`);
    else if(!((info.id > 0) && (await bcrypt.compare(obj.password, info.password)))) return error(res, "Username or password incorrect!");
    else {
        let result = await sql(`SELECT admin_privilleges FROM users WHERE user_id = ?;`, [`${info.id}`]);
        return res.json({
            token: await jwt.sign({
                data: {
                    id: info.id,
                    isAdmin: result[0].admin_privilleges
                }
            }, 'secret', { expiresIn: '7d' })
        });
    }

    
}

async function post_register(req, res){
    var obj = req.body;

    if((obj.username && obj.email && obj.password && obj.passwordVerify && obj.first_name && obj.last_name ) == null) return error(res, `Uncomplete object.`);
    else if((obj.username && obj.email && obj.password && obj.passwordVerify && obj.first_name && obj.last_name) == "") return error(res, `Fill in all blanks.`);
    else if(await isUsernameTaken(obj.username)) return error(res, 'Username is already taken.');
    else if(!(await isUsernameValid(obj.username))) return error(res, 'Username is invalid');
    else if(await isEmailTaken(obj.email)) return error(res, 'Email is already in use.');
    else if(!(await isEmailValid(obj.email))) return error(res, 'Email is invalid.');
    else if(obj.password != obj.passwordVerify) return error(res, `Passwords don't match.`);
    else {
        obj.username = await obj.username.toLowerCase();
        obj.email = await obj.email.toLowerCase();
        let passwordHashed = bcrypt.hashSync(obj.password, saltRounds);
        let result = await db.awaitQuery(`INSERT INTO users (username, email, password, first_name, last_name) VALUES (?,?,?,?,?);`, [`${obj.username}`, `${obj.email}`, `${passwordHashed}`, `${obj.first_name}`, `${obj.last_name}`]);
        if(result.affectedRows == 0) return error(res, "Something went wrong!");
    }
    return res.json({});
}

async function get_search(req, res){
    var obj = req.query;

    let result = await sql(`
    SELECT DISTINCT r.recipe_id FROM recipes r
    WHERE r.title LIKE ?;`
    ,[`%${obj.title}%`]);

    let recipes = [];
    for(var element of result) {
        var recipe = await getRecipe(element.recipe_id, await getMyUser(req));
        recipes.push(recipe);
    };
    res.send(recipes);
}

async function get_search_categorie(req, res){
    var obj = req.query;
    if(obj.categories == null || obj.categories == "") return error(res, 'No categories are given.')
    obj.categories = await obj.categories.split(',');
    var myUser = await getMyUser(req);

    let result = await sql(`
    SELECT DISTINCT r.recipe_id FROM recipes r
        LEFT JOIN recipe_has_categories rc ON r.recipe_id = rc.recipe_id
        LEFT JOIN e_categories c ON rc.category_id = c.category_id
    WHERE c.category_label IN (?);`
    ,[obj.categories]);

    res.json(await Promise.all(result.map(element => getRecipe(element.recipe_id, myUser))));
}

async function get_favourites_search_categorie(req, res){
    var obj = req.query;
    if(obj.categories == null || obj.categories == "") return error(res, 'No categories are given.')
    obj.categories = await obj.categories.split(',');
    var myUser = await getMyUser(req);

    let result = await sql(`
    SELECT DISTINCT r.recipe_id FROM recipes r
        JOIN user_has_favourites uf ON uf.recipe_id = r.recipe_id AND uf.user_id = ?
        LEFT JOIN recipe_has_categories rc ON r.recipe_id = rc.recipe_id
        LEFT JOIN e_categories c ON rc.category_id = c.category_id
    WHERE c.category_label IN (?);`
    ,[myUser.id, obj.categories]);

    res.json(await Promise.all(result.map(element => getRecipe(element.recipe_id, myUser))));
}

async function get_ingredients(req, res){
    res.json(await sql(`SELECT DISTINCT lower(label) ingredient FROM recipe_has_ingredients;`).then(results => results.map(value => value.ingredient)));
}

async function get_fridge(req, res){
    var obj = req.query;
    if(obj.ingredients == null && obj.ingredients == "") return error(res, `No ingredients selected`);
    if(obj.categories != null && obj.categories != "") return await get_fridge_categories(req, res);
    obj.ingredients = await obj.ingredients.toLowerCase().split(',');
    

    let sql_recipes = await sql(`
        SELECT R.recipe_id as id, LOWER(GROUP_CONCAT(RI2.label)) as labels
        from recipe_has_ingredients RI2
        join recipes R on RI2.recipe_id  = R.recipe_id
        GROUP BY R.recipe_id;`,[]);

    let recipes = []

    for(var recipe of sql_recipes){
        recipe.labels = await recipe.labels.split(',');

        if(obj.ingredients.every(x => recipe.labels.includes(x))) recipes.push(await getRecipe(recipe.id, await getMyUser(req)));
    }

    res.send(recipes);
}

async function get_fridge_categories(req, res){
    var obj = req.query;
    if(obj.ingredients == null) return error(res, `No ingredients selected`);
    if(obj.categories == null) return error(res, `No categories given`);
    obj.ingredients = await obj.ingredients.toLowerCase().split(',');
    obj.categories = await obj.categories.toLowerCase().split(',');

    let sql_recipes = await sql(`
        SELECT R.recipe_id as id, LOWER(GROUP_CONCAT(RI2.label)) as labels
        from (select ri.* from recipe_has_ingredients ri
            join recipe_has_categories rc on rc.recipe_id = ri.recipe_id
            join e_categories c on c.category_id = rc.category_id
            where c.category_label in (?)) RI2
        join recipes R on RI2.recipe_id  = R.recipe_id
        GROUP BY R.recipe_id;`,[obj.categories]);

    let recipes = []

    for(var recipe of sql_recipes){
        recipe.labels = await recipe.labels.split(',');

        if(obj.ingredients.every(x => recipe.labels.includes(x))) recipes.push(await getRecipe(recipe.id, await getMyUser(req)));
    }

    res.send(recipes);
}

async function isUsernameTaken(username){
    let sql_statement = `SELECT EXISTS(SELECT 1 FROM users WHERE username = ?) as val;`;
    let sql_values = [`${username}`];
    let result = await sql(sql_statement, sql_values);
    return result[0].val == 1 ? true : false;
}

async function isEmailTaken(email){
    let sql_statement = `SELECT EXISTS(SELECT 1 FROM users WHERE email = ?) as val;`;
    let sql_values = [`${email}`];
    let result = await sql(sql_statement, sql_values);
    return result[0].val == 1 ? true : false;
}

async function getUserInformation(user){
    let sql_statement = ``;
    if(await isEmailValid(user) && await isEmailTaken(user)) sql_statement = `SELECT user_id, password FROM users WHERE email = ?;`;
    else if(await isUsernameValid(user) && await isUsernameTaken(user)) sql_statement = `SELECT user_id, password FROM users WHERE username = ?;`;
    else return { id: 0 };

    let result = await sql(sql_statement, [`${user}`]);
    return {
        id: result[0].user_id,
        password: result[0].password
    };
}

function isUsernameValid(username){
    const regex_username = '';
    return true;
    return regex_username.test(String(username).toLowerCase());
}

function isEmailValid(email){
    const regex_email = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex_email.test(String(email).toLowerCase());
}

async function getMyUser(req){
    var auth = req.headers["authorization"];
    if((auth) == null || (auth) == "") return null;
    else {
        var user = await decodeToken(auth);
        if(await sql(`SELECT EXISTS(SELECT 1 FROM users WHERE user_id = ?) as value;`, [`${data.id}`]).then(results => results.length == 0)) return null;
        return user.data;
    }
}

async function decodeToken(token){
    return await jwt.decode(token, 'secret', true, {algorithm: 'HS256'});
}

async function sql(statemant, values = []){
    return new Promise((resolve, reject) => {
        db.query(statemant, values, (error, rows) => { 
            if (error) return reject(error);
            return resolve(rows);
        });
    });
}

async function error(res, message = "", code = 400){
    return res.status(code).send({error: message});
}